$(function() {
    var popup1_closed = true;
    var popup2_closed = true;
    var popup_form_closed = true;
    var report_missing = $('#report_miss');
    if (report_missing) {
        report_missing.click(function(ev) {
            close_all();
            popup_form();
        });
    }

    var popup1_div = null;
    function popup1() {
        popup1_div = $("#popup1");
        if (popup1_div) {
            popup1_div.dialog({
                autoOpen: true,
                modal: true,
                draggable: false,
                resizable: false,
                width: 500,
                height: 363,
                buttons: [{text: "Continue", click: function() {
                            close_all();
                            // set timeout for the popup2
                            setTimeout(function() {
                                popup2();
                            }, 30000);
                        }}],
                dialogClass: 'popup1',
                open: function(event, ui) {
                    $('body').addClass('noscroll');
                }
            });
            popup1_closed = false;
        }

    }

    var popup2_div = null;
    function popup2() {
        close_all();
        popup2_div = $('#popup2');
        popup2_div.dialog({
            autoOpen: true,
            modal: true,
            draggable: false,
            resizable: false,
            width: 500,
            height: 193,
            dialogClass: 'popup2',
            open: function(event, ui) {
                $('body').addClass('noscroll');
            }
        });
        popup2_closed = false;
    }

    var popup_form_div = null;
    function popup_form() {
        close_all();
        popup_form_div = $('#popup_form_div');
        popup_form_div.dialog({
            autoOpen: true,
            modal: true,
            draggable: false,
            resizable: false,
            width: 500,
            height: 'auto',
            dialogClass: 'popup_form',
            buttons: [{text: "Submit", click: function(ev) {
                        ajax_request(ev);
                    }}],
            open: function(event, ui) {
                $('body').addClass('noscroll');
            }
        });
        $('.popup_form_bottom').remove();
        $('.popup_form').append('<div class="popup_form_bottom"></div>');
        popup_form_closed = false;
        $('#popup_form_text textarea').focus();
    }
    function ajax_request(ev) {
        var popup_form_text = $('#popup_form_text');
        popup_form_text.append('<div id="popup_loading"></div>');
        $.ajax({
            'type': 'POST',
            'url': '/report-missing-info',
            'data': $('#popup_form').serialize(),
            'cache': false,
            'context': this
        }).done(function(data) {
            popup_form_text.children('div').remove(); // remove loader gif
            $('.popup_form_err').remove();
            var popup_div = $('#popup_form_div');
            var err_count = 0;
            $.each(data, function(i, err) {
                popup_div.prepend('<div class="popup_form_err"><b>' + err.key + ':</b>&nbsp;' + err.val + '</div>');
                err_count++;
            });
            // reposition the popup
            //popup_form_div.dialog("option", "position", "center");
            popup_form_text.children('textarea').focus();
            if (err_count == 0) {
                setTimeout(function() {
                    popup_form_div.dialog('close');
                }, 1000);
            }
        });
    }

    function close_all() {
        if (!popup1_closed) {
            popup1_div.dialog('close');
            popup1_closed = true;
        }
        if (!popup2_closed) {
            popup2_div.dialog('close');
            popup2_closed = true;
        }
        if (!popup_form_closed) {
            popup_form_div.dialog('close');
            popup_form_closed = true;
        }
        $('body').removeClass('noscroll');
    }


    $('.popup_close').click(function(ev) {
        close_all();
    });

    // start
    setTimeout(function() {
        close_all();
        popup1();
    }, 15000);
});
