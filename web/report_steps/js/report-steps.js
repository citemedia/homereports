//popups
$(function() {
  var popup1_closed = true;
  var popup2_closed = true;
  var popup_form_closed = true;
  var report_missing = $('#report_miss');
  if (report_missing) {
    report_missing.click(function(ev) {
      close_all();
      popup_form();
    });
  }

  var popup1_div = null;
  function popup1() {
    popup1_div = $("#popup1");
    if (popup1_div) {
      popup1_div.dialog({
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false,
        width: 500,
        height: 363,
        buttons: [{text: "Continue", click: function() {
                                       close_all();
                                       // set timeout for the popup2
                                       setTimeout(function() {
                                         popup2();
                                       }, 30000);
                                     }}],
        dialogClass: 'popup1',
        open: function(event, ui) {
          $('body').addClass('noscroll');
        },
        close: function(event, ui) {
          $('body').removeClass('noscroll');
        }
      });
      popup1_closed = false;
    }
  }

  var popup2_div = null;
  function popup2() {
    close_all();
    popup2_div = $('#popup2');
    popup2_div.dialog({
      autoOpen: true,
      modal: true,
      draggable: false,
      resizable: false,
      width: 500,
      height: 193,
      dialogClass: 'popup2',
      open: function(event, ui) {
        $('body').addClass('noscroll');
      },
      close: function(event, ui) {
        $('body').removeClass('noscroll');
      }
    });
    popup2_closed = false;
  }

  var popup_form_div = null;
  function popup_form() {
    close_all();
    popup_form_div = $('#popup_form_div');
    popup_form_div.dialog({
      autoOpen: true,
      modal: true,
      draggable: false,
      resizable: false,
      width: 500,
      height: 'auto',
      dialogClass: 'popup_form',
      buttons: [{text: "Submit", click: function(ev) {
                                   ajax_request(ev);
                                 }}],
      open: function(event, ui) {
        $('body').addClass('noscroll');
      },
      close: function(event, ui) {
        $('body').removeClass('noscroll');
      }
    });
    $('.popup_form_bottom').remove();
    $('.popup_form').append('<div class="popup_form_bottom"></div>');
    popup_form_closed = false;
    $('#popup_form_text textarea').focus();
  }
  function ajax_request(ev) {
    var popup_form_text = $('#popup_form_text');
    popup_form_text.append('<div id="popup_loading"></div>');
    $.ajax({
      'type': 'POST',
      'url': '/report-missing-info',
      'data': $('#popup_form').serialize(),
      'cache': false,
      'context': this
    }).done(function(data) {
      popup_form_text.children('div').remove(); // remove loader gif
      $('.popup_form_err').remove();
      var popup_div = $('#popup_form_div');
      var err_count = 0;
      $.each(data, function(i, err) {
        popup_div.prepend('<div class="popup_form_err"><b>' + err.key + ':</b>&nbsp;' + err.val + '</div>');
        err_count++;
      });
      // reposition the popup
      //popup_form_div.dialog("option", "position", "center");
      popup_form_text.children('textarea').focus();
      if (err_count == 0) {
        setTimeout(function() {
          popup_form_div.dialog('close');
        }, 1000);
      }
    });
  }

  function close_all() {
    if (!popup1_closed) {
      popup1_div.dialog('close');
      popup1_closed = true;
    }
    if (!popup2_closed) {
      popup2_div.dialog('close');
      popup2_closed = true;
    }
    if (!popup_form_closed) {
      popup_form_div.dialog('close');
      popup_form_closed = true;
    }
    $('body').removeClass('noscroll');
  }


  $('.popup_close').click(function(ev) {
    close_all();
  });

  // start
  setTimeout(function() {
    close_all();
    //popup1();
  }, 15000);

});

var sv_service, sv_pan;

function streetview(arealoc) {
  sv_service = new google.maps.StreetViewService();
  sv_service.getPanoramaByLocation(arealoc, 500,
                                   function(data, status) {
                                     // sv_pan = new google.maps.StreetViewPanorama(document.getElementById('birdeye'), {
                                     //   position: data.location.latLng,
                                     //   pov: {
                                     //     heading: 34,
                                     //     pitch: 10
                                     //   }
                                     // });
                                     if (data) {
                                       sv_pan = new google.maps.StreetViewPanorama(document.getElementById('propdetailsmap'), {
                                         position: data.location.latLng,
                                         pov: {
                                           heading: 34,
                                           pitch: 10
                                         }
                                       });
                                       //            sv_pan.setZoom(0);
                                       //            sv_pan.getVisible(true);
                                     }
                                   });
}

var crime_markers = [];
function loadCrimes(center, radius, options) {
  if (typeof (radius) === 'undefined' || radius === null) {
    radius = 0.020;
  }
  $.each(crime_markers, function(index, value) {
    value.setMap(null);
  });
  crime_markers = [];

  var url = "http://api.spotcrime.com/crimes.json?";
  url += "lat=" + center.lat();
  url += "&lon=" + center.lng();
  url += "&radius=" + radius;
  url += "&callback=?";
  url += "&key=MLC-restricted-key";

  $.getJSON(url, function(data) {
    if (data === null) {
      $('#table_container').html('');
      return;
    }
    if (data.crimes.length < 5) {
      retry = options.retry || 0;
      $.extend(options, {retry: retry + 1});

      if (retry < 5) {
        loadCrimes(center, radius * 2, options);
      } else {
        removeMovementEventListener();
        addMovementEventListener();
      }
      return;
    }

    var tableHTML = "<table>";
    var bounds = new google.maps.LatLngBounds();
    var totalCount = data.crimes.length;

    /* render points on map */
    $.each(data.crimes, function(index, crime) {
      /* render marker */
      var marker = new google.maps.Marker({
        map: localCrimesMap,
        position: new google.maps.LatLng(crime.lat, crime.lon),
        icon: imageForType(crime.type, true),
        title: crime.type,
        zIndex: totalCount - index
      });
      bounds.extend(marker.getPosition());
      var infowindow_content = "<div class='infowindow'><h4>" + crime.type + "</h4><p>" + crime.address + "</p><p>" + crime.date + "</p><p><a href='" + crime.link + "' target='_blank'>More information at SpotCrime.com</a></p></div>";
      google.maps.event.addListener(marker, 'click', function(event) {
        infowindow.setContent(infowindow_content);
        infowindow.open(localCrimesMap, marker);
      });
      crime_markers.push(marker);
      tableHTML += "<tr id='" + crime.cdid + "' onclick='window.open(\"" + crime.link + "\")'><td class='type'>";
      tableHTML += "<img src='" + imageForType(crime.type) + "' alt='" + crime.type + "'/>";
      tableHTML += "</td><td class='description'>";
      tableHTML += crime.type;
      tableHTML += " @ ";
      tableHTML += crime.address;
      tableHTML += "</td><td class='date'>";
      tableHTML += crime.date;
      tableHTML += "</td></tr>";
    });
    tableHTML += "</table>";
    $('#table_container').html(tableHTML);

    removeMovementEventListener();
    if (options.fitBounds) {
      localCrimesMap.fitBounds(bounds);
    }
    addMovementEventListener();
  });
}
function removeMovementEventListener() {
  if (movementListener) {
    google.maps.event.removeListener(movementListener);
    movementListener = null;
  }

  if (movementListeners.length > 0) {
    for (var i = 0; i < movementListeners.length; i++) {
      google.maps.event.removeListener(movementListeners[i]);
    }
    movementListeners = [];
  }
}
function addMovementEventListener() {
  removeMovementEventListener();
  var onMovementEvent = function() {
    if (movementTimeout) {
      clearTimeout(movementTimeout);
    }
    movementTimeout = setTimeout(movementComplete, 500);
  };

  movementListeners = [
    google.maps.event.addListener(localCrimesMap, 'zoom_changed', onMovementEvent),
    google.maps.event.addListener(localCrimesMap, 'dragend', onMovementEvent)
  ];
}

function shortVal(num) {
  return num.toString().substring(0,9);
}

// retarget map, reload crimes
function movementComplete() {
  if (geocoder) {
    var latLng = localCrimesMap.getCenter();
    $("#address").val(shortVal(latLng.lat()) + ", " + shortVal(latLng.lng()));
    codeAddress({fitBounds: false});
  }
}

function imageForType(type, shadow) {
  var root = imgroot;
  if (shadow) {
    root += "/web/report_steps/images/localcrime/shadow/";
  }
  else {
    root += "/web/report_steps/images/localcrime/no_shadow/";
  }
  return root + {
    "Theft": 'theft',
    "Robbery": 'robbery',
    "Burglary": 'burglary',
    "Vandalism": 'vandalism',
    "Shooting": 'shooting',
    "Arson": 'arson',
    "Arrest": 'arrest',
    "Assault": 'assault',
    "Other": 'other'
  }[type] + '.png';
}

function initializemaps() {

  infowindow = new google.maps.InfoWindow({
    content: '',
    maxWidth: 400
  });

  geocoder = new google.maps.Geocoder();

  geocoder.geocode({'address': address, 'region': 'us'}, function(results, status) {

    arealoc = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

    if (status === google.maps.GeocoderStatus.OK) {
      var downloadMapProp = {
        zoom: 18,
        center: arealoc,
        mapTypeId: google.maps.MapTypeId.SATELLITE
      };
      downloadMap = new google.maps.Map(document.getElementById("birdeye"), downloadMapProp);

      var localCrimesProp = {
        zoom: 15,
        center: arealoc,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      localCrimesMap = new google.maps.Map(document.getElementById("localcrimes"), localCrimesProp);

      loadCrimes(arealoc, null, {fitBounds: true});

    } else {
      alert("Sorry this address does not exist in our records. Please try again.");
      setTimeout(function() {
        window.location.replace("/");
      }, 3000);
    }

    streetview(arealoc);
  });
}

var crimeChart = null;
function initCrimeChart() {
  //AmCharts.ready(function() {
  crimeChart = new AmCharts.AmSerialChart();
  crimeChart.dataProvider = crimeChartData;
  crimeChart.categoryField = "type";
  var graph = new AmCharts.AmGraph();
  graph.valueField = "value";
  graph.colorField = "color";
  graph.type = "column";
  graph.fillAlphas = 1;
  graph.balloonText = "[[category]]: <b>[[value]]</b>";
  graph.lineAlpha = 0;
  crimeChart.addGraph(graph);
  crimeChart.startDuration = 0.5;
  crimeChart.write('crimerisk');
  //});
}

var weatherChart = null;
function initWeatherRisksChart() {
  //AmCharts.ready(function() {
  weatherChart = new AmCharts.AmSerialChart();
  weatherChart.dataProvider = weatherRisksData;
  weatherChart.categoryField = "type";
  var graph = new AmCharts.AmGraph();
  graph.valueField = "value";
  graph.colorField = "color";
  graph.type = "column";
  graph.fillAlphas = 1;
  graph.balloonText = "[[category]]: <b>[[value]]</b>";
  graph.lineAlpha = 0;
  weatherChart.addGraph(graph);
  weatherChart.startDuration = 0.5;
  weatherChart.write('weatherrisk');
  //});
}

var householdsChart = null;
function initHouseholdsIncomeChart() {
  //AmCharts.ready(function() {
  householdsChart = new AmCharts.AmSerialChart();
  householdsChart.dataProvider = householdsChartData;
  householdsChart.categoryField = 'range';
  householdsChart.categoryAxis.position = 'left';
  householdsChart.categoryAxis.gridPosition = 'start';
  householdsChart.valueAxes = [{axisAlpha: 0, position: 'right'}];
  householdsChart.rotate = true;
  householdsChart.plotAreaBorderAlpha = 0;
  var graph = new AmCharts.AmGraph();
  graph.valueField = "value";
  graph.colorField = "color";
  graph.type = "column";
  graph.fillAlphas = 1;
  graph.balloonText = "[[category]]: <b>[[value]]</b>";
  graph.lineAlpha = 0;
  householdsChart.startDuration = 0.5;
  householdsChart.addGraph(graph);
  householdsChart.write('householdincome');
  //});
}

var educationChart = null;
function initEducationChart() {
  //AmCharts.ready(function() {
  educationChart = new AmCharts.AmPieChart();
  educationChart.dataProvider = educationChartData;
  educationChart.titleField = "type";
  educationChart.valueField = "value";
  educationChart.colorField = "color";
  educationChart.balloonText = '[[title]]<br><span style="font-size:14px"><b>[[value]]</b> ([[percents]]%)</span>';
  educationChart.depth3D = 15;
  // var legend = new AmCharts.AmLegend();
  // legend.markerType = "circle";
  // legend.position = "right";
  // legend.marginRight = 80;
  // legend.autoMargins = false;
  // chart.addLegend(legend);
  educationChart.startDuration = 0.3;
  educationChart.write('educationlevel');
  //});
}

var transportChart = null;
function initTransportationChart() {
  //AmTransportCharts.ready(function() {
  transportChart = new AmCharts.AmPieChart();
  transportChart.dataProvider = transportationData;
  transportChart.titleField = "type";
  transportChart.valueField = "value";
  transportChart.colorField = "color";
  transportChart.balloonText = '[[title]]<br><span style="font-size:14px"><b>[[value]]</b> ([[percents]]%)</span>';
  transportChart.startDuration = 0.3;
  transportChart.depth3D = 15;
  transportChart.write('transportation');
  //});
}

var ownershipChart = null;
function initOwnershipChart() {
  ownershipChart = new AmCharts.AmPieChart();
  ownershipChart.dataProvider = ownershipData;
  ownershipChart.titleField = "type";
  ownershipChart.valueField = "value";
  ownershipChart.colorField = "color";
  ownershipChart.balloonText = '[[title]]<br><span style="font-size:14px"><b>[[value]]</b> ([[percents]]%)</span>';
  ownershipChart.depth3D = 15;
  ownershipChart.startDuration = 0.3;
  ownershipChart.write('prop_ownership');
}


function initSchoolsChart() {
  google.setOnLoadCallback(drawSchoolsChart);
  function drawSchoolsChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'School');
    data.addColumn('string', 'Grades');
    data.addColumn('string', 'Distance');
    data.addColumn('string', 'Rating');

    data.addRows(schoolsData);
    data.setColumnProperty(3, 'style', 'text-align:center;');

    for (var i = 0; i < data.getNumberOfRows(); i++) {
      data.setCell(i, 1, undefined, undefined, {style: 'text-align:center;'});
      data.setCell(i, 2, undefined, undefined, {style: 'text-align:center;'});
      data.setCell(i, 3, undefined, undefined, {style: 'text-align:center;'});
    }

    var table = new google.visualization.Table(document.getElementById('schools'));
    table.draw(data, {showRowNumber: false, allowHtml: true});
  }
}

function setCookie(key, value) {
  document.cookie = key+"="+value;
}

function codeAddress(opts) {

  var address = $("#address").val(),
      options = {fitBounds: true};

  // set default options
  $.extend(options, opts);

  if (geocoder) {
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        $("#alert").html("");
        $("#alert").hide();
        setCookie('address', address);
        //window.location = '#' + escape(address);

        loadCrimes(results[0].geometry.location, null, options);

      } else {
        $("#alert").html("Sorry, we could not understand your location. Please try a different address.");
        $("#alert").show();
      }
    });
  }
}

var watermarked = true;
function clearWatermark() {
  if (watermarked === true) {
    watermarked = false;
    $("#address").val('');
  }
}

// scrolledIntoView jQuery plugin
(function($){

  var $window = $(document), _buffer = null,
      watchscrolled = {}, pluginName = 'scrolledIntoView',
      active = false;

  $.fn['scrolledIntoView'] = function(options) {

      var settings = {
        scrolledin: null,
        scrolledout: null
      };

      var opts = $.extend({}, settings, options);
      this.each( function () {
        var $el = $(this), instance = $.data( this, pluginName );
        if ( instance ) {
          instance.options = opts;
        } else {
          $.data( this, pluginName, monitorscrolled( $el, opts ) );
        }
      });

    if (!active) {
      active = true;
      $.fn.scrolledIntoView.start();
    }

    return this;
  }

  $.fn.scrolledIntoView.start = function() {
    $window.on('scroll', watchScrolling);
    active = true;
  }

  $.fn.scrolledIntoView.stop = function() {
    $window.off('scroll');
    active = false;
  }

  $.fn.scrolledIntoView.remove = function(name) {
    $.each(watchscrolled, function(i, v) {
      if (v.options.name === name) {
        $.removeData( v.element, pluginName);
        delete watchscrolled[i];
      }
    });
  }

  function watchScrolling ( e ) {
    if ( !_buffer ) {
      _buffer = setTimeout(function () {checkInView( e ); _buffer = null;}, 1000);
    }
  }

  function monitorscrolled(element, options) {
    var item = { element: element, options: options, invp: false };
    watchscrolled[options.name] = item;
    return item;
  }

  function checkInView( e ) {
    if ($.isEmptyObject(watchscrolled)) {
      $.fn.scrolledIntoView.stop();
      return;
    }
    $.each(watchscrolled, function () {
      if ( test( this.element ) ) {
        if ( !this.invp ) {
          this.invp = true;
          if ( this.options.scrolledin ) this.options.scrolledin.call( this.element, e );
          //this.element.trigger( 'scrolledin', e );
        }
      } else if ( this.invp ) {
        this.invp = false;
        if ( this.options.scrolledout ) this.options.scrolledout.call( this.element, e );
        //this.element.trigger( 'scrolledout', e );
      }
    });
  }

  function test($el) {
    var docViewTop = $window.scrollTop(),
        docViewBottom = docViewTop + screen.availHeight,
        elemTop = $el.offset().top,
        elemBottom = elemTop + $el.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop) );
  }

}(jQuery));

function init() {

  initCrimeChart();
  initHouseholdsIncomeChart();
  initWeatherRisksChart();
  initSchoolsChart();
  initOwnershipChart();

  if ($.browser.msie && parseInt($.browser.version, 10) === 8) {
    initEducationChart();
    initTransportationChart();
  } else {
    $('#crimerisk').scrolledIntoView({name: 'crimerisk',
                                      scrolledin: function() {
                                        crimeChart.animateAgain();
                                        $.fn.scrolledIntoView.remove('crimerisk');
                                      }});
    $('#weatherrisk').scrolledIntoView({name: 'weatherrisk',
                                        scrolledin: function() {
                                          weatherChart.animateAgain();
                                          $.fn.scrolledIntoView.remove('weatherrisk');
                                        }});
    $('#householdincome').scrolledIntoView({name: 'householdincome',
                                            scrolledin: function() {
                                              householdsChart.animateAgain();
                                              $.fn.scrolledIntoView.remove('householdincome');
                                            }});
    $('#educationlevel').scrolledIntoView({name: 'educationlevel',
                                           scrolledin: function() {
                                             initEducationChart();
                                             $.fn.scrolledIntoView.remove('educationlevel');
                                           }});
    $('#transportation').scrolledIntoView({name: 'transportation',
                                           scrolledin: function() {
                                             initTransportationChart();
                                             $.fn.scrolledIntoView.remove('transportation');
                                           }});
    $('#prop_ownership').scrolledIntoView({name: 'ownership',
                                           scrolledin: function() {
                                             initOwnershipChart();
                                             $.fn.scrolledIntoView.remove('ownership');
                                           }});
  }
}
