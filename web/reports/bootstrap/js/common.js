

var loan_slider = $('#UpsellForm_percentDown').slider({
    formater: function(value) { return 'Current value: '+value; } }).on('slide', function(ev){
        $('#loan-percent-down-display .value').html(loan_slider.getValue());
    }
).data('slider');

// Custom: Change btn-group to act like a form select field.
$('.btn-group .dropdown-menu a').click(function(e) {
    var dropdown_group = $(this).parents('.btn-group');
    var dropdown_group_id = $(dropdown_group).attr('id');
    var dropdown_label = $(dropdown_group).find('.dropdown-toggle').find('.dd-label');
    if (dropdown_label != null) {
        var this_html = $(this).html();
        if (dropdown_group_id == 'website-pages') { // Custom/Hack
            if (this_html != 'Maximum') {
                var num_pages = (this_html * 1);
                var num_pages_label = num_pages + ' Page';
                if (num_pages > 1) num_pages_label += 's';
                dropdown_label.html(num_pages_label);
            }
            else {
                dropdown_label.html(this_html);
            }
        }
        else { // Default (without Custom/Hack)
            dropdown_label.html(this_html);
        }
        switch(dropdown_group_id) {
            case 'website-budget': website_budget = this_html; break;
            case 'website-pages': website_pages = this_html; break;
            case 'website-type': website_type = this_html; break;

            case 'hosting-budget': hosting_budget = this_html; break;
            case 'seo-budget': seo_budget = this_html; break;

            case 'seo-type': seo_type = this_html; break;
        }
    }
    $(dropdown_group).find('.dropdown-menu').children('li').removeClass('active');
    $(this).parent('li').addClass('active');
});
