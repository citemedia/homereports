<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use CM\Form\OrderForm3;

$app->match('/acheckout', function (Request $request) use ($app) {
  return checkout_a($app, $request, ['nextUrl' => 'specialoffer']);
})->bind('acheckout')->requireHttps();

$app->match('/bcheckout', function (Request $request) use ($app) {
  return checkout_b($app, $request, ['nextUrl' => 'specialoffer']);
})->bind('bcheckout')->requireHttps();

$app->match('/select-payment-a', function (Request $request) use ($app) {
  return select_payment_a($app, $request, ['nextUrl' => 'new-or-existing']);
})->bind('select-payment-a');

$app->match('/select-payment-b', function (Request $request) use ($app) {
  return select_payment_b($app, $request, ['nextUrl' => 'new-or-existing']);
})->bind('select-payment-b');

function checkout_a(Application $app, Request $request, Array $options)
{
  if ($app['security']->isGranted('ROLE_USER')) {
    $app['session']->set('order', true);
    return $app->redirect('report');
  }

//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

//  if ($app['session']->get('order')) {
//    return $app->redirect($options['nextUrl']);
//  }

  $form = $app['form.factory']
      ->createBuilder(new OrderForm3())
      ->getForm();
  
  $db = new Database\CheckoutSplit($app);

  if ('POST' == $request->getMethod()) {

    $form->submit($request);

    if ($form->isValid()) {

      $data = $form->getData();
      $app['session']->set('checkout_data', $data);
      $app['session']->set('upsell_decline', false);
      $app['session']->set('customerData', $data);
      $app['session']->set('order', true);

      $r = new RedirectResponse($app['url_generator']->generate($options['nextUrl']));
      $db->updateVisitor($r, 'acheckout', 'yes', $data['report']);
      return $r;
      
    }
  }
  
  $html = $app['twig']->render('checkout_split/checkout.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
  $r = new Response($html);
  $db->updateVisitor($r, 'acheckout', 'no', null);

  return $r;
}

function checkout_b(Application $app, Request $request, Array $options)
{
  if ($app['security']->isGranted('ROLE_USER')) {
    $app['session']->set('order', true);
    return $app->redirect('report');
  }

//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

//  if ($app['session']->get('order')) {
//    return $app->redirect($options['nextUrl']);
//  }

  $form = $app['form.factory']
      ->createBuilder(new OrderForm3())
      ->getForm();
  
  $db = new Database\CheckoutSplit($app);

  if ('POST' == $request->getMethod()) {

    $form->submit($request);

    if ($form->isValid()) {

      $data = $form->getData();
      $app['session']->set('checkout_data', $data);
      $app['session']->set('upsell_decline', false);
      $app['session']->set('customerData', $data);
      $app['session']->set('order', true);

      $r = new RedirectResponse($app['url_generator']->generate($options['nextUrl']));
      $db->updateVisitor($r, 'bcheckout', 'yes', $data['report']);
      return $r;
      
    }
  }
  
  $html = $app['twig']->render('checkout_split/checkout.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
  $r = new Response($html);
  $db->updateVisitor($r, 'bcheckout', 'no', null);

  return $r;
}

function select_payment_a(Application $app, Request $request, Array $options)
{
//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

  $app['session']->set('checkout_version', 'acheckout');
  
  if ('POST' == $request->getMethod()) {
    return $app->redirect($options['nextUrl']);
  }

  return $app['twig']->render('split/select_payment.html.twig', ['options' => $options]);
}

function select_payment_b(Application $app, Request $request, Array $options)
{
//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

  $app['session']->set('checkout_version', 'bcheckout');
  
  if ('POST' == $request->getMethod()) {
    return $app->redirect($options['nextUrl']);
  }

  return $app['twig']->render('checkout_split/select_payment.html.twig', ['options' => $options]);
}
