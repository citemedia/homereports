<?php
namespace CM;

class FormChoices
{
    public static function getIncome()
    {
        return array(1=>'Stock Market', 2=>'Real Estate', 3=>'Infopreneuring',4=>'Internet Income', 5=>'Residual Income');
    }

    public static function getChoices()
    {
        return array(1=>'Yes', 2=>'No', 3=>'Maybe');
    }

    public static function getTitle()
    {
        return array('Mr.', 'Ms.', 'Mrs.', 'Miss');
    }

    public static function getConfirm()
    {
        return array(1=>'Yes', 2=>'No');
    }

    public static function getBool()
    {
        return array(1=>'Yes', 0=>'No');
    }


    public static function getStates()
    {
        return array(
            "" => "Choose",
            "AL" => "Alabama",
            "AK" => "Alaska",
            "AZ" => "Arizona",
            "AR" => "Arkansas",
            "CA" => "California",
            "CO" => "Colorado",
            "CT" => "Connecticut",
            "DE" => "Delaware",
            "DC" => "District Of Columbia",
            "FL" => "Florida",
            "GA" => "Georgia",
            "HI" => "Hawaii",
            "ID" => "Idaho",
            "IL" => "Illinois",
            "IN" => "Indiana",
            "IA" => "Iowa",
            "KS" => "Kansas",
            "KY" => "Kentucky",
            "LA" => "Louisiana",
            "ME" => "Maine",
            "MD" => "Maryland",
            "MA" => "Massachusetts",
            "MI" => "Michigan",
            "MN" => "Minnesota",
            "MS" => "Mississippi",
            "MO" => "Missouri",
            "MT" => "Montana",
            "NE" => "Nebraska",
            "NV" => "Nevada",
            "NH" => "New Hampshire",
            "NJ" => "New Jersey",
            "NM" => "New Mexico",
            "NY" => "New York",
            "NC" => "North Carolina",
            "ND" => "North Dakota",
            "OH" => "Ohio",
            "OK" => "Oklahoma",
            "OR" => "Oregon",
            "PA" => "Pennsylvania",
            "RI" => "Rhode Island",
            "SC" => "South Carolina",
            "SD" => "South Dakota",
            "TN" => "Tennessee",
            "TX" => "Texas",
            "UT" => "Utah",
            "VT" => "Vermont",
            "VA" => "Virginia",
            "WA" => "Washington",
            "WV" => "West Virginia",
            "WI" => "Wisconsin",
            "WY" => "Wyoming"
        );
    }

    public static function getMonths()
    {
        return array(
            '1' => '01 / Jan',
            '2' => '02 / Feb',
            '3' => '03 / Mar',
            '4' => '04 / Apr',
            '5' => '05 / May',
            '6' => '06 / Jun',
            '7' => '07 / Jul',
            '8' => '08 / Aug',
            '9' => '09 / Sep',
            '10' => '10 / Oct',
            '11' => '11 / Nov',
            '12' => '12 / Dec'
        );
    }

    public static function getYears($length=10)
    {
        $year = date('Y');
        $short = date('y');

        return array_combine(
            range($short, $short + $length),
            range($year, $year + $length)
        );
    }



}
