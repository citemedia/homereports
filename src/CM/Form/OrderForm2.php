<?php

namespace CM\Form;

use CM\Form\Type\CCVFormType;
use CM\FormChoices;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class OrderForm2 extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text', [
                'label' => 'First Name:',
                'attr' => ['class' => 'form-txtbox', 'minlength' => 2],
                'constraints' => [new NotBlank()],
            ])
            ->add('lastName', 'text', [
                'label' => 'Last Name:',
                'attr' => ['class' => 'form-txtbox'],
                'constraints' => [new NotBlank()],
            ])
            ->add('emailAddress', 'text', [
                'label' => 'Email:',
                'attr' => ['class' => 'form-txtbox email required'],
                'constraints' => [new NotBlank()],
            ])
            ->add('postcode', 'text', [
                'label' => 'Postal/Zip Code:',
                'attr' => ['class' => 'form-txtbox required numbers', 'minlength'=>5, 'maxlength'=>5],
                'constraints' => [new Length(['min'=>5, 'max'=>5]), new Regex(['pattern'=>"/[\d]{5}/", 'message'=>'Zipcode can contain only digits'])],
            ])
            ->add('ccNumber', 'text', [
                'label' => 'Card Number:',
                'attr' => ['class' => 'form-txtbox digits numbers', 'maxlength' => '16', 'minlength'=>'16' ],
                'constraints' => [new NotBlank()],
            ])
            ->add('ccType', 'choice', [
                'choices' => $this->get_cc_types(),
                'required' => true,
                //'expanded' => true,
                'label' => 'Card Type',
                'attr' => ['class' => 'form-txt-field'],
                'constraints' => [new NotBlank()],
            ])
            ->add('ccExpMonth', 'choice', [
                'choices' => FormChoices::getMonths(),
                'label' => 'Expiration Date',
                'attr' => ['class' => 'form-txtbox-2'],
                'constraints' => [new NotBlank()],
            ])
            ->add('ccExpYear', 'choice', [
                'choices' => FormChoices::getYears(),
                'label' => 'Expiration Year',
                'attr' => ['class' => 'form-txtbox-3'],
                'constraints' => [new NotBlank()],
            ])
            ->add('ccCVV', new CCVFormType(), [
                'label' => 'CVV2/CVC2:',
                'attr' => ['class' => 'form-txtbox-2 digits required numbers',
                    'minlength'=>3, 'maxlength'=>3],
                'constraints' => [new NotBlank()],
            ]);
    }

    private function get_cc_types()
    {
        return [
            '2' => 'Visa',
            '3' => 'MasterCard'
        ];
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'billingCountry' => 'US',
            'noCCType' => false,
            'area' => 'US',
            'type' => 0,
            'validation_groups' => [Constraint::DEFAULT_GROUP],
        ));

    }

    public function getName()
    {
        return 'OrderForm';
    }
}
