<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class SearchForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street', 'text', [
                'attr' => ['class' => 'search-txtfield'],
                'constraints' => [new NotBlank()],
                'required' => false,
                'label' => 'Street Address'
            ])
            ->add('city', 'text', [
                'attr' => ['class' => 'search-txtfield'],
                'constraints' => [new NotBlank()],
                'required' => false,
                'label' => 'City'
            ])
            ->add('zip', 'text', [
                'attr' => ['class' => 'search-form-txtbox-2'],
                'constraints' => [new NotBlank(), new Length(['max' => 5, 'min' => 5]), new Type(['type' => 'numeric'])],
                'required' => false,
                'label' => 'Zip'
            ])
            ->add('state', 'choice', [
                'choices' => ['AL' => 'Alabama', 'AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas',
                    'CA' => 'California', 'CO' => 'Colorado', 'CT' => 'Connecticut',
                    'DE' => 'Delaware', 'DC' => 'District of Columbia',
                    'FL' => 'Florida', 'GA' => 'Georgia', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois',
                    'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky',
                    'LA' => 'Louisiana', 'ME' => 'Maine', 'MD' => 'Maryland',
                    'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota',
                    'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana',
                    'NE' => 'Nebraska', 'NV' => 'Nevada', 'New Hampshire' => 'New Hampshire',
                    'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York',
                    'NC' => 'North Carolina', 'ND' => 'North Dakota', 'OH' => 'Ohio',
                    'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania',
                    'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota',
                    'TN' => 'Tennessee', 'TX' => 'Texas', 'UT' => 'Utah', 'VT' => 'Vermont',
                    'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia',
                    'WI' => 'Wisconsin', 'WY' => 'Wyoming', 'Other' => 'Other'],
                'empty_value' => 'Select State',
                'attr' => ['class' => 'search-form-txtbox'],
                'constraints' => [new NotBlank()],
                'required' => false,
                'label' => 'State'
            ]);
    }

    public function getName()
    {
        return 'SearchForm';
    }
}
