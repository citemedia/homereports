<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class LoginForm extends AbstractType
{
    const NAME = 'LoginForm';
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'text', [
                'constraints' => [new NotBlank(), new Email()],
                'required' => false,
            ])
            ->add('password', 'password', [
                'constraints' => [new NotBlank()],
                'required' => false,
            ]);
    }

    public function getName()
    {
        return LoginForm::NAME;
    }
}
