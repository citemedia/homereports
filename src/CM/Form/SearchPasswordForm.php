<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class SearchPasswordForm extends AbstractType
{
    const NAME = 'SearchPasswordForm';
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'text', [
                'constraints' => [new NotBlank(), new Email()],
                'required' => true,
            ]);
    }

    public function getName()
    {
        return SearchPasswordForm::NAME;
    }
}
