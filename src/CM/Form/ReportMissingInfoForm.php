<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReportMissingInfoForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'constraints' => [new NotBlank()],
                'required' => true,
                'label' => 'Name',
            ])
             ->add('email', 'text', [
                'label' => 'Email',
                'attr' => ['class' => ''],
                'constraints' => [new NotBlank()],
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'attr' => ['class' => ''],
                'constraints' => [new NotBlank()],
            ]);
    }

    public function getName()
    {
        return 'ReportMissingInfoForm';
    }
}
