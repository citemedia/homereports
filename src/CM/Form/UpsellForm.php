<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;

class UpsellForm extends AbstractType
{
    private $emailAddress;

    function __construct($emailAddress = null)
    {
        $this->emailAddress = $emailAddress;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailAddress', 'text', [
                'constraints' => [new NotBlank()],
                'required' => true,
                'data' => $this->emailAddress
            ])
            ->add('password', 'repeated', [
                'type' => 'password',
                'constraints' => [new NotBlank()],
                'required' => true
            ])
        ;
    }

    public function getName()
    {
        return 'UpsellForm';
    }
}
