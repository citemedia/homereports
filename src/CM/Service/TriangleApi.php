<?php

namespace CM\Service;

use Silex\Application;
use SoapClient;

class TriangleApi extends SoapClient
{

    private $app;

    function __construct(Application $app)
    {
        $this->app = $app;
        parent::__construct($this->app['config']['Triangle']['wsdl'], [
            'location' => $this->app['config']['Triangle']['url'],
            'trace' => true,
            'cache_wsdl' => 1,
            'soap_version' => SOAP_1_2
        ]);
    }
    
    public function simpleOrder($data, $address)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'amount' => $conf['simple_order_amount'],
            'shipping' => 0,
            'shippingSpecified' => false,
            'productTypeID' => $conf['main_product_group_id'],
            'productTypeIDSpecified' => true,
            'productID' => 94,
            'productIDSpecified' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'City',
            'state' => 'CA',
            'zip' => $data['postcode'],
            'phone' => '',
            'email' => $data['emailAddress'],
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'sendConfirmationEmail' => false,
            'customField1' => $address,
        ];
        
        $r = $this->charge($var);
        //$r = $this->__soapCall('charge', array('parameters'=>$var));
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function simpleOrder1($data, $address)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'amount' => 0.95,
            'shipping' => 0,
            'shippingSpecified' => false,
            'productTypeID' => 6,
            'productTypeIDSpecified' => true,
            'productID' => 94,
            'productIDSpecified' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'City',
            'state' => 'CA',
            'zip' => $data['postcode'],
            'phone' => '',
            'email' => $data['emailAddress'],
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'sendConfirmationEmail' => false,
            'customField1' => $address,
        ];
        
        $r = $this->charge($var);
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function simpleOrder2($data, $address)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'amount' => 0.25,
            'shipping' => 0,
            'shippingSpecified' => false,
            'productTypeID' => 6,
            'productTypeIDSpecified' => true,
            'productID' => 95,
            'productIDSpecified' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'City',
            'state' => 'CA',
            'zip' => $data['postcode'],
            'phone' => '',
            'email' => $data['emailAddress'],
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'sendConfirmationEmail' => false,
            'customField1' => $address,
        ];
        
        $r = $this->charge($var);
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function simpleOrderCheckoutDiscount($data, $address)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'amount' => 0.25,
            'shipping' => 0,
            'shippingSpecified' => false,
            'productTypeID' => 6,
            'productTypeIDSpecified' => true,
            'productID' => 95,
            'productIDSpecified' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'City',
            'state' => 'CA',
            'zip' => $data['postcode'],
            'phone' => '',
            'email' => $data['emailAddress'],
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'sendConfirmationEmail' => false,
            'customField1' => $address,
        ];
        
        $r = $this->charge($var);
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    // cmt 
    public function isCCDupe($creditCard)
    {
        $conf = $this->app['config']['Triangle'];
        $var = [
            'creditCard' => $creditCard,
            'productID' => $conf['trialPackageID']
            //'productID' => '92'
        ];
        
        $r = $this->IsCreditCardDupe($var);
        //var_dump($r);die();
        if ($r) {
            $r = $r->IsCreditCardDupeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }

    
    public function subscribe($data, $address)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'planID' => $conf['subscription_plan_id'],
            'trialPackageID' => $conf['trialPackageID'],
            'chargeForTrial' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'string',
            'state' => 'string',
            'zip' => $data['postcode'],
            'country' => 'USA',
            'phone' => '',
            'email' => $data['emailAddress'],
            'sendConfirmationEmail' => true,
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'description' => 'order',
            'customField1' => $address,
        ];
        
        $r = $this->createSubscriptionCustom($var);
        
        if ($r) {
            $r = $r->CreateSubscriptionCustomResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function setProspectID($data)
    {
        $prospectID = $this->app['session']->get('prospectID');
        if (!$prospectID) {
            $conf = $this->app['config']['Triangle'];
        
            $var = [
                'username' => $conf['user'],
                'password' => $conf['pass'],
                'firstName' => $data['firstName'],
                'lastName' => $data['lastName'],
                'address1' => 'string',
                'address2' => '',
                'city' => 'string',
                'state' => 'string',
                'zip' => $data['postcode'],
                'country' => 'USA',
                'phone' => '',
                'email' => $data['emailAddress'],
                'ip' => $this->app['request']->getClientIp(),
                'affiliate' => '',
                'subAffiliate' => '',
                'internalID' => ''
            ];

            $r = $this->createProspect($var);
            if ($r->CreateProspectResult->State == "Success") {
                $prospectID = $r->CreateProspectResult->ReturnValue->ProspectID;
                $this->app['session']->set('prospectID', $prospectID);
            } else {
                $prospectID = false;
            }
        }
        
        return $prospectID;
    }
}