<?php

namespace CM\Service;

use Silex\Application;
use SoapClient;

class UpsellBilling extends SoapClient
{

    private $app;

    function __construct(Application $app)
    {
        $this->app = $app;
        parent::__construct($this->app['config']['Triangle']['wsdl'],
                            [
                             'location' => $this->app['config']['Triangle']['url'],
                             //'location' => 'https://ndp2.trianglecrm.com/campaign_api/FormService.svc/ProcessBilling?registrationID=',
                             'trace' => true,
                             'cache_wsdl' => 1,
                             'soap_version' => SOAP_1_2
                             ]);
    }
    
    public function upsellaSimple($data, $amount, $productID, $campaign)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }

        $conf = $this->app['config']['Triangle'];
        $campaignSpecified = false;
        if ($campaign) {
            $campaignSpecified = true;
        }        

        $var = [
                'username' => $conf['user'],
                'password' => $conf['pass'],
                'amount' => $amount,
                'shipping' => 0,
                'shippingSpecified' => false,
                'productTypeID' => $conf['main_product_group_id'],
                'productTypeIDSpecified' => true,
                'productID' => $productID,
                'productIDSpecified' => true,
                'campaignID' => $campaign,
                'campaignIDSpecified' => $campaignSpecified,
                'firstName' => $data['firstName'],
                'lastName' => $data['lastName'],
                'address1' => 'empty',
                'address2' => '',
                'city' => 'City',
                'state' => 'CA',
                'zip' => $data['postcode'],
                'phone' => '',
                'email' => $data['emailAddress'],
                'ip' => $this->app['request']->getClientIp(),
                'affiliate' => '',
                'subAffiliate' => '',
                //'internalID' => '',
                'prospectID' => $this->app['session']->get('prospectID'),
                'paymentType' => $data['ccType'],
                'creditCard' => $data['ccNumber'],
                'cvv' => $data['ccCVV'],
                'expMonth' => $data['ccExpMonth'],
                'expYear' => $data['ccExpYear'],
                'sendConfirmationEmail' => true,
                'customField1' => $this->app['session']->get('searchString')
                ];

        $r = $this->charge($var);
        //$r = $this->__soapCall('charge', array('parameters'=>$var));
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }

    public function upsellaSubscription($data, $trialID, $planID, $campaign)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }

        $conf = $this->app['config']['Triangle'];
        $campaignSpecified = false;
        if ($campaign) {
            $campaignSpecified = true;
        }        
        
        $var = [
                'username' => $conf['user'],
                'password' => $conf['pass'],
                'planID' => $planID,
                'trialPackageID' => $trialID,
                'chargeForTrial' => true,
                'campaignID' => $campaign,
                'campaignIDSpecified' => $campaignSpecified,
                'firstName' => $data['firstName'],
                'lastName' => $data['lastName'],
                'address1' => 'empty',
                'address2' => '',
                'city' => 'string',
                'state' => 'string',
                'zip' => $data['postcode'],
                'country' => 'USA',
                'phone' => '',
                'email' => $data['emailAddress'],
                'ip' => $this->app['request']->getClientIp(),
                'affiliate' => '',
                'subAffiliate' => '',
                'internalID' => '',
                'prospectID' => $this->app['session']->get('prospectID'),
                'paymentType' => $data['ccType'],
                'creditCard' => $data['ccNumber'],
                'cvv' => $data['ccCVV'],
                'expMonth' => $data['ccExpMonth'],
                'expYear' => $data['ccExpYear'],
                'description' => 'order',
                'sendConfirmationEmail' => true,
                'customField1' => $this->app['session']->get('searchString')
                ];

        $r = $this->createSubscriptionCustom($var);
        
        if ($r) {
            $r = $r->CreateSubscriptionCustomResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function isCCDupe($creditCard)
    {
        $conf = $this->app['config']['Triangle'];
        $var = [
                'creditCard' => $creditCard,
                'productID' => $conf['trialPackageID']
                //'productID' => '92'
                ];
        
        $r = $this->IsCreditCardDupe($var);
        //var_dump($r);die();
        if ($r) {
            $r = $r->IsCreditCardDupeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function setProspectID($data)
    {
        $prospectID = $this->app['session']->get('prospectID');
        if (!$prospectID) {
            $conf = $this->app['config']['Triangle'];
        
            $var = [
                    'username' => $conf['user'],
                    'password' => $conf['pass'],
                    'firstName' => $data['firstName'],
                    'lastName' => $data['lastName'],
                    'address1' => 'string',
                    'address2' => '',
                    'city' => 'string',
                    'state' => 'string',
                    'zip' => $data['postcode'],
                    'country' => 'USA',
                    'phone' => '',
                    'email' => $data['emailAddress'],
                    'ip' => $this->app['request']->getClientIp(),
                    'affiliate' => '',
                    'subAffiliate' => '',
                    'internalID' => ''
                    ];

            $r = $this->createProspect($var);
            if ($r->CreateProspectResult->State == "Success") {
                $prospectID = $r->CreateProspectResult->ReturnValue->ProspectID;
                $this->app['session']->set('prospectID', $prospectID);
            } else {
                $prospectID = false;
            }
        }
        
        return $prospectID;
    }
}