<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use CM\Form\EbookSpecial;
use CM\Service\EbookSpecialCRM;
use CM\Service\TriangleApi;

$app->get('/ebookspecialintro', function (Request $request) use ($app) {
    return ebookspecialintro($app, $request);
})->bind('ebookspecialintro');

$app->match('/ebookspecialselect', function (Request $request) use ($app) {
    return ebookspecialselect($app, $request);
})->bind('ebookspecialselect');

$app->get('/ebookspecialthx', function (Request $request) use ($app) {
    return ebookspecialthx($app, $request);
})->bind('ebookspecialthx');

$app->match('/guide-to-buying-your-dream-home', function(Request $request) use ($app) {
    $size = filesize("../web/ebookspecial/books/Guide_to_Buying_Your_Dream_Home.pdf");

    return $app->stream(function() {
            readfile("../web/ebookspecial/books/Guide_to_Buying_Your_Dream_Home.pdf");
        }, 200, array(
            'Content-Type' => 'application/pdf',
            'Content-length' => $size,
            'Content-Disposition' => 'attachment; filename="Guide_to_Buying_Your_Dream_Home.pdf"',
            'Content-Transfer-Encoding' => 'binary'
    ));
})->bind('dream-home');

$app->match('/how-to-make-your-home-sell', function(Request $request) use ($app) {
    $size = filesize("../web/ebookspecial/books/How_to_Make_Your_Home_Sell.pdf");

    return $app->stream(function() {
            readfile("../web/ebookspecial/books/How_to_Make_Your_Home_Sell.pdf");
        }, 200, array(
            'Content-Type' => 'application/pdf',
            'Content-length' => $size,
            'Content-Disposition' => 'attachment; filename="How_to_Make_Your_Home_Sell.pdf"',
            'Content-Transfer-Encoding' => 'binary'
    ));
})->bind('home-sell');

function ebookspecialintro($app, $request)
{
    $app['session']->set('ebook_decline', true);
    
    /* if (!$app['session']->get('order')) { */
    /*     return $app->redirect('checkout'); */
    /* } */
    
    if ($app['session']->get('purchased_ebookspecial', false)) {
        return $app->redirect($app['url_generator']->generate('report'));
    }
    
    return $app['twig']->render('ebookspecial/ebookspecialintro.html.twig');
}

function ebookspecialselect($app, $request)
{
    /* if (!$app['session']->get('order')) { */
    /*     return $app->redirect('checkout'); */
    /* } */
    
    $form = $app['form.factory']
        ->createBuilder(new EbookSpecial())
        ->getForm();

    if ('POST' == $request->getMethod()) {
        $form->submit($request);
        if ($form->isValid()) {
            $data = $form->getData();
            if (!$data['emailAddress'] && $data['ebooks'] == 1) {
                $error = 'Email should not be blank.';
                return $app['twig']->render('ebookspecial/ebookspecialselect.html.twig', ['form' => $form->createView(), 'error' => $error]);
            }
            elseif ($data['ebooks'] == 2) {
                $api = new TriangleApi($app);
                if (!$app['session']->get('ebook_decline', false)) {
                    $data = $app['session']->get('checkout_data');
                    $result = $api->simpleOrderCheckout43_2($data, $app['session']->get('searchString'));
                    if ($result->State == 'Error') {
                        //var_dump( 'decline error' );die();
                        // error
                    } else {
                        $app['session']->set('ebook_decline', true);
                    } 
                    return $app->redirect($app['url_generator']->generate('report'));
                }
                return $app->redirect($app['url_generator']->generate('report'));
            }
            else {
                $crm = new EbookSpecialCRM($app);
                $data = $app['session']->get('checkout_data');
                if (!$crm->isCCDupe($data['ccNumber'])) {
                    $result = $crm->buyEbook($data, 4.99, true);
                    if ($result->State == 'Error') {
                        return $app['twig']->render('ebookspecial/ebookspecialselect.html.twig', ['form' => $form->createView(), 'error' => 'Something went wrong']);
                    }
                }
                $app['session']->set('purchased_ebookspecial', true);
                return $app->redirect($app['url_generator']->generate('ebookspecialthx'));
            }
        }
    }

    return $app['twig']->render('ebookspecial/ebookspecialselect.html.twig', ['form' => $form->createView(), 'error' => '']);
}

function ebookspecialthx($app, $request)
{
    return $app['twig']->render('ebookspecial/ebookspecialthx.html.twig');
}
