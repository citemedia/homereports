<?php
namespace Cloud;

class TwigAssetExtension extends \Twig_Extension
{
    public function __construct(CloudExtension $cfExtension, \Silex\Application $app)
    {
        $this->internalCache = array();
        $this->cfExtension = $cfExtension;
        $this->app = $app;
    }

    public function getFilters()
    {
        return array('asset' => new \Twig_Filter_Method($this, 'asset'));
    }

    public function asset($path)
    {
        //if (file_exists($this->app['cf.assets.base_dir'] . $path)) {
        //    $path .= '?v=' . filemtime($this->app['cf.assets.base_dir'] . $path);
        //} else {
        //    $path .= '?v=1';
        //}

        //return '/assets/' . ltrim($path, '/');

        /*legacy code used for cloud, should be removed*/
       
        
        if (isset($this->app['dm.config']->globalSettings['serve_assets_from_local']) && $this->app['dm.config']->globalSettings['serve_assets_from_local']
             || !isset($this->app['dm.config']->settings['use_cdn']) ) {
            if (file_exists(__DIR__.'/../../resources/public/'.$path)) {
            $path .= '?v=1';
        } else {
            $path .= '?v=333';
        }

        return '/assets/' . ltrim($path, '/');
        }

        // Cache this result since it will be called for every asset
        if ( ! isset($this->isSecureRequest)) {
            $this->isSecureRequest = $this->app['request']->isSecure();
        }

        // guarantee prefix
        $path = ltrim($path, '/');

        $internalCacheKey = ($this->isSecureRequest ? 'http:' : 'https:') . $path;

        if (isset($this->internalCache[$internalCacheKey])) {
            return $this->internalCache[$internalCacheKey];
        }
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        $assetVersion = $this->cfExtension->getAssetVersion($this->app['cf.assets.base_dir']);

        $redisCacheKey = "homeincome:twig:asset:{$this->isSecureRequest}:$assetVersion:$path";
        $uri = $this->app['redis']->get($redisCacheKey);
        if ($uri) {
            return $this->internalCache[$internalCacheKey] = $uri;
        }

        // End of hot path, this code wont run very often
        try {
            $container = $this->cfExtension->getCdnContainer($path);
            $object = $container->get_object($path);
        } catch (\NoSuchObjectException $e) {
            throw new \NoSuchObjectException($e->getMessage() . " in '{$container->name}'");
        }

        $uri = $this->isSecureRequest ? $object->public_ssl_uri() : $object->public_uri();

        $this->app['redis']->set($redisCacheKey, $uri);

        return $this->internalCache[$internalCacheKey] = $uri;
    }

    public function getName()
    {
        return 'asset';
    }

    private function getAsset($path){
        
    }
}
