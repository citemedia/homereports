<?php

use CM\Form\SearchForm;
use Silex\Application;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use CM\Service\TriangleApi;
use Api\Weather\Weather;
use Api\GreatSchools\GreatSchoolsAPI;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Api\Trulia\Trulia;
use Api\ZillowApi\ZillowApi;

/* $app->match('/checkout43', function (Request $request) use ($app) { */
/*     return split_homepage($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'checkout43']); */
/* })->bind('checkout43'); */

/* $app->match('/declined', function (Request $request) use ($app) { */
/*     return declined($app, ['nextUrl' => 'report']); */
/* })->bind('declined'); */

$app->match('/propertyrecord77', function (Request $request) use ($app) {
    return split_homepage($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'propertyrecord77_10']);
})->bind('homepage');

/* $app->match('/warning', function (Request $request) use ($app) { */
/*     return warning($app, $request, ['nextUrl' => 'select-payment']); */
/* })->bind('warning'); */

/* $app->match('/select-payment', function (Request $request) use ($app) { */
/*     return select_payment($app, $request, ['nextUrl' => 'new-or-existing']); */
/* })->bind('select-payment'); */

/* $app->match('/new-or-existing', function (Request $request) use ($app) { */
/*     return new_or_existing($app, $request, ['nextUrl' => 'feedback']); */
/* })->bind('new-or-existing'); */

/* $app->match('/feedback', function (Request $request) use ($app) { */
/*     return feedback($app, $request, ['nextUrl' => 'checkout']); */
/* })->bind('feedback'); */

$app->match('/feedback_a', function (Request $request) use ($app) {
    return feedback($app, $request, ['nextUrl' => 'checkout_a']);
})->bind('feedback_a');
$app->match('/feedback_b', function (Request $request) use ($app) {
    return feedback($app, $request, ['nextUrl' => 'checkout_b']);
})->bind('feedback_b');
$app->match('/feedback_c', function (Request $request) use ($app) {
    return feedback($app, $request, ['nextUrl' => 'checkout_c']);
})->bind('feedback_c');

$app->match('/report', function (Request $request) use ($app) {
    return report($app, $request, ['nextUrl' => 'homepage']);
})->bind('report')->requireHttp();

$app->match('/reportest', function (Request $request) use ($app) {
    return report($app, $request, ['nextUrl' => 'homepage', 'test'=>true]);
})->bind('reportest')->requireHttp();

$app->match('/homeinsurance', function (Request $request) use ($app) {
    return homeinsurance($app, $request, []);
})->bind('homeinsurance')->requireHttp();

$app->match('/reportupgrade1', function (Request $request) use ($app) {
    return reportupgrade1($app, $request, []);
})->bind('reportupgrade1');
$app->match('/reportupgrade2', function (Request $request) use ($app) {
    return reportupgrade2($app, $request, []);
})->bind('reportupgrade2');
function reportupgrade1(Application $app, Request $request, $options)
{
    return $app['twig']->render("upsell_split/reportupgrade1.html.twig", [
    ]);
}
function reportupgrade2(Application $app, Request $request, $options)
{
    return $app['twig']->render("upsell_split/reportupgrade2.html.twig", [
    ]);
}

$app->match('/mortgage-rates', function (Request $request) use ($app) {
    return mortgage_rates($app, $request, []);
})->bind('mortgage_rates')->requireHttp();

function report(Application $app, Request $request, $options)
{
    if (!isset($options['test'])) {
        if (!$app['session']->get('searchData')) {
            return redirectHome($app);
        }

        if (!$app['session']->get('order') && !$app['security']->isGranted('ROLE_USER')) {
            return redirectHome($app);
        }
    }
    
    $sess = $app['session'];

    $reportMissingInfoForm = $app['form.factory']
            ->createBuilder(new CM\Form\ReportMissingInfoForm())
            ->getForm();

    $generic = true;
    $data = $sess->get('searchData', []);
    
    $data['city'] = isset($data['city']) ? $data['city'] : 'Las Vegas';
    $data['state'] = isset($data['state']) ? $data['state'] : 'NV';
    $data['zip'] = $data['zipcode'] = isset($data['zip']) ? $data['zip'] : '89107';
    $data['street'] = isset($data['street']) ? $data['street'] : '2500 Wimbledon';
    //$data['street'] = isset($data['street']) ? $data['street'] : '1210 First avenue';

    $info = [];

    $md = new Api\MelissaData\MelissaData2('106897455');
    $mdav = $md->getAddressValidation($data);
    $onboard = new Api\Onboardinformatics\Onboardinformatics($app);
    $zillow = new ZillowApi($app['zwsid']);
    //$trulia = new Trulia('7pu5qg9smvb57qrr296krr2g');
    //$tdata = $trulia->getZipCodeStats($data['zip']);
    //$tdata = $trulia->getZipCodeStats('98121');
    //var_dump( $tdata->response );die();

    //$dm = $zillow->GetDemographics(['zip'=>$data['zip']]);
    //var_dump( $dm->response->pages );die(); 
    
    if ($mdav->success) {
        $mprop = $md->getProperty((string)$mdav->response->Record->Address->AddressKey);
        if ($mprop->success) {
            //ldd($mprop->response->Record);
            $info['bedrooms'] = $mprop->response->Record->Building->BedRooms;
            $info['bathrooms'] = $mprop->response->Record->Building->TotalBaths;
            $info['year_built'] = (string)$mprop->response->Record->Building->YearBuilt;
            $info['stories'] = $mprop->response->Record->Building->Stories;
            $info['acreage'] = $mprop->response->Record->Lot->Acreage;
            $info['square_footage'] = $mprop->response->Record->SquareFootage->UniversalBuilding;
            $info['address'] = $mprop->response->Record->PropertyAddress->Address;
            $info['city'] = $mprop->response->Record->PropertyAddress->City;
            $info['state'] = $mprop->response->Record->PropertyAddress->State;
            $info['zip'] = $mprop->response->Record->PropertyAddress->Zip;
            $info['owner'] = $mprop->response->Record->Owner->Name;
            $info['tax_amount'] = $mprop->response->Record->Values->TaxAmount;
            $info['tax_year'] = $mprop->response->Record->Values->TaxYear;
            $generic = false;
        } else {
            $generic = true;
            
            /* $resp = $onboard->avm([ */
            /*     'street' => $data['street'], */
            /*     'city' => $data['city'], */
            /*     'state' => $data['state'], */
            /*     'zip' => $data['zip'] */
            /* ]); */
        }
    }

    setlocale(LC_MONETARY, 'en_US.utf8');
    
    $searchString = $sess->get('searchString') ? $sess->get('searchString') : "{$data['street']}, {$data['city']} {$data['state']}, {$data['zip']}";
    $geocode = $onboard->getGeocode($searchString);
    $loc = $geocode[0]->geometry->location;
    $WKTString = "POINT({$loc->lng} {$loc->lat})";
    $latlong = ['latitude' => $loc->lat, 'longitude' => $loc->lng];

    //$app['melissaData'] = ['zip' => $md->getFromZip(getZipfromGeocode($geocode[0]->address_components))];
    
    $areas = $onboard->getArea('Hierarchy/Lookup', ['WKTString' => $WKTString]);
    $geo_key = pickArea($areas); // AreaId
    $community = $onboard->getCommunityAreaFull(['AreaId' => $geo_key]);

    if (!$generic) {
        $homerangepercent = $mprop->response->Record->Values->CalculatedTotalValue / 100;
        $info['homerangemedian'] = str_replace('USD ', '', money_format('%i', (double)$mprop->response->Record->Values->CalculatedTotalValue));
        $info['homerangelow'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 70));
        $info['homerangehigh'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 130));
        
        if ((double)$info['bedrooms'] == 0.0) {$info['bedrooms'] = '';}
        if ((double)$info['bathrooms'] == 0.0) {$info['bathrooms'] = '';}
        if ((double)$info['stories'] == 0.0) {$info['stories'] = '';}
        if ((string)$info['square_footage'] == '') {$info['square_footage'] = '';}
        if (((int)date('Y') - 3) > (int)$info['tax_year']) {$info['tax_amount'] = '';}
        $owner_words = explode(' ', $info['owner']);
        $info['owner'] = '';
        foreach ($owner_words as $word) {
            $info['owner'] .= ' '.ucfirst(strtolower($word));
        }
        $info['owner'] = trim($info['owner']);
    } else {
        $homerangepercent = $community->medsaleprice / 100;
        $info['homerangemedian'] = str_replace('USD ', '', money_format('%i', (double)$community->medsaleprice));
        $info['homerangelow'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 70));
        $info['homerangehigh'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 130));
    }

    $info['medsaleprice'] = str_replace('USD ', '', money_format('%i', (double)$community->medsaleprice));
    $info['hmvcymort'] = str_replace('USD ', '', money_format('%i', (double)$community->hmvcymort));

    /* $ps = $onboard->getPropertySearch([ */
    /*     'TransactionType' => 'F0000', */
    /*     'Period' => 'Annual', */
    /*     'AreaId' => $geo_key */
    /* ]); */
    /* $property_search = array_pop($ps); */

    $app['weather'] = new Weather($data['state'], $data['city']);
    
    $greatschools_key = 'shmu5qtv08rquudifxsfggxx';

    $gs = new GreatSchoolsAPI($greatschools_key);
    $app['nearbySchools'] = $gs->nearbySchools($data['city'], $data['state']);

    if ($generic) { $tmpl = 'report_steps_generic.html.twig'; }
    else $tmpl = 'report_steps.html.twig';
    
    return $app['twig']->render($tmpl, [
                'options' => $options,
                'reportMissingInfoForm' => $reportMissingInfoForm->createView(),
                'latlong' => $latlong,
                'firstName' => $sess->get('customerData')['firstName'],
                'emailAddress' => $sess->get('customerData')['emailAddress'],
                'searchString' => $searchString,
                'data' => $data,
                'info' => $info,
                'generic' => true,
                //'property_search' => $property_search, 
                'community' => $community
    ]);
}

function homeinsurance(Application $app, Request $request, $options)
{
    $params = [
        'bwapsstate' => '',
        'bwapcurrentlyinsured' => '',
        'bwapsagedriver' => '',
        'bwapsincidenthistory' => '',
        'bwapshomeowner' => '',
        'bwapsmultidriverhouseholds' => ''
    ];
    $params = array_merge($params, $request->query->all());
    //ldd($params);
     
    $zipcode = $request->query->get('ZipCode');
    return $app['twig']->render('homeinsurance.html.twig', [
        'bwapsstate' => $params['bwapsstate'], 
	    'bwapszip' => $zipcode,
        'bwapcurrentlyinsured' => $params['bwapcurrentlyinsured'],
        'bwapsagedriver' => $params['bwapsagedriver'],
        'bwapsincidenthistory' => $params['bwapsincidenthistory'],
        'bwapshomeowner' => $params['bwapshomeowner'],
        'bwapsmultidriverhouseholds' => $params['bwapsmultidriverhouseholds']
    ]);
}

function mortgage_rates($app, $request, $options)
{
    $zipcode = $request->get('zipcode');
    $loan_type = $request->get('loan_type');
    $credit_score = $request->get('credit_score');
    $property_type = $request->get('property_type');
    
    return $app['twig']->render("mortgage_rates.html.twig", [
        'zipcode'=>$zipcode,
        'loan_type'=>$loan_type,
        'credit_score'=>$credit_score,
        'property_type'=>$property_type
    ]);
}

function pickArea($areas)
{
    $geo_key = '';
    foreach ($areas as $area) {
        if ($area->type == 'Zip Code') {
            $geo_key = $area->geo_key;
        }
    }
    if (!$geo_key) {
        foreach ($areas as $area) {
            if ($area->type == 'County') {
                $geo_key = $area->geo_key;
            }
        }
    }

    return $geo_key;
}

/* function declined(Application $app, Array $options) */
/* { */
/*     $api = new TriangleApi($app); */
/*     $data = $app['session']->get('checkout_data'); */
/*     if (!$app['session']->get('upsell_decline', false)) { */
/*         $result = $api->simpleOrderCheckout43_2($data, $app['session']->get('searchString')); */
/*         if ($result->State == 'Error') { */
/*             //var_dump( 'decline error' );die(); */
/*             // error */
/*         } else { */
/*             $app['session']->set('upsell_decline', true); */
/*         } */
/*     } */

/*     if ($app['session']->get('funnel') == 'propertyrecord77_10') { */
/*         return $app->redirect($app['url_generator']->generate('ebookspecialintro')); */
/*         //return $app->redirect($app['url_generator']->generate('report')); */
/*     } */
/*     return $app->redirect($options['nextUrl']); */
/* } */

/* function feedback(Application $app, Request $request, Array $options) */
/* { */
/*     if (!$app['session']->get('searchData')) { */
/*         return redirectHome($app); */
/*     } */

/*     if ('POST' == $request->getMethod()) { */
/*         return $app->redirect($options['nextUrl']); */
/*     } */

/*     return $app['twig']->render('split/feedback.html.twig', ['options' => $options]); */
/* } */

/* function new_or_existing(Application $app, Request $request, Array $options) */
/* { */
/*     if (!$app['session']->get('searchData')) { */
/*         return redirectHome($app); */
/*     } */

/*     if ('POST' == $request->getMethod()) { */
/*         return $app->redirect($options['nextUrl']); */
/*     } */

/*     $app['session']->set('cardtype', $request->get('cardTypeR')); */

/*     return $app['twig']->render('split/new_or_existing.html.twig', ['options' => $options]); */
/* } */

/* function select_payment(Application $app, Request $request, Array $options) */
/* { */
/*     if (!$app['session']->get('searchData')) { */
/*         return redirectHome($app); */
/*     } */

/*     if ('POST' == $request->getMethod()) { */
/*         return $app->redirect($options['nextUrl']); */
/*     } */

/*     return $app['twig']->render('split/select_payment.html.twig', ['options' => $options]); */
/* } */

/* function warning(Application $app, Request $request, Array $options) */
/* { */
/*     if (!$app['session']->get('searchData')) { */
/*         return redirectHome($app); */
/*     } */

/*     if ('POST' == $request->getMethod()) { */
/*         return $app->redirect($options['nextUrl']); */
/*     } */

/*     return $app['twig']->render('split/warning.html.twig', ['options' => $options]); */
/* } */

/* function split_homepage(Application $app, Request $request, Array $options) */
/* { */
/*     $session = $app['session']; */

/*     $session->set('funnel', $options['funnel']); */
/*     $session->set('property', null); */
/*     $session->set('searchData', null); */
/*     $session->set('searchString', null); */
/*     $session->set('order', null); */
/*     $session->set('upsellOrder', null); */

/*     /\** @var Form $form *\/ */
/*     $form = $app['form.factory'] */
/*             ->createBuilder(new SearchForm()) */
/*             ->getForm(); */

/*     if ('POST' == $request->getMethod()) { */

/*         $form->submit($request); */

/*         if ($form->isValid()) { */
/*             $data = $form->getData(); */
/*             $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip']; */

/*             $session->set('searchData', $data); */
/*             $session->set('searchString', $searchString); */

/*             return $app->redirect($options['nextUrl']); */
/*         } */
/*     } */

/*     return $app['twig']->render( */
/*         'split/homepage.html.twig', [ */
/*             'searchForm' => $form->createView(), */
/*             'options' => $options */
/*         ] */
/*     ); */
/* } */

function getZipfromGeocode($addr_components) {
    foreach ($addr_components as $k=>$v) {
        if (in_array('postal_code', $v->types)) {
            return $v->long_name;
        }
    }
}
