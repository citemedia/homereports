<?php

namespace Splits\UpsellSplit\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UpsellForm extends AbstractType
{
    const NAME = 'UpsellForm';
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'text', [
                'constraints' => [new NotBlank(), new Email()],
                'required' => false,
            ])
            ->add('price', 'hidden', [
                'required' => true,
                'data'=> '7'
            ]);
    }

    public function getName()
    {
        return self::NAME;
    }
}
