<?php
namespace Api\GreatSchools;

class GreatSchoolsAPI
{
    private $url = "http://api.greatschools.org/schools";

    private $api_key;

    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }

    public function nearbySchools($city, $state, $zip = null, $address = null, $lat = null, $lon = null, $minimum = 5, $limit = 8, $radius = 5)
    {
        $url = $this->url . "/nearby?key={$this->api_key}&city=" . urlencode($city) . "&state=" . urlencode($state);
        if ($zip) $url .= "&zip=" . urlencode($zip);
        if ($address) $url .= "&address=" . urlencode($address);
        if ($lat && $lon) {
            $url .= '&lat=' . urlencode($lat) . '&lon' . urlencode($lon);
        }
        $url .= '&minimumSchools=' . urlencode($minimum) . '&limit=' . urlencode($limit);

        return simplexml_load_string(file_get_contents($url));
    }
}