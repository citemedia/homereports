<?php
namespace Api\WalkScore;

class WalkScore
{
	private $url = "http://api.walkscore.com/score?format=json";
    private $apiKey = '325cd4223ed2b40c34038f507f771ec0';
    
    public function results($lat, $lon, $address='Beverly Hills, CA')
    {
        $url = $this->url."&wsapikey={$this->apiKey}&lat=".urlencode($lat)."&lon=".urlencode($lon)."&address=".urlencode($address).'';
        
        return json_decode(file_get_contents($url)) ;
    }
    
}