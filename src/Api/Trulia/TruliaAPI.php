<?php
namespace Api\Trulia;

class TruliaAPI
{
    private $url = "http://api.trulia.com/webservices.php";
    private $api_key;
    
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }
    
    /**
     * 
     * @param type $city
     * @param type $state
     * @return type
     * 
     * funcs:
     * getCitiesInState	Retrieves all cities in a state.
     * getCountiesInState	Retrieves all counties in a state.
     * getNeighborhoodsInCity	Retrieves all Neighborhoods in a city.
     * getStates	Retrieves all 50 states.
     * getZipCodesInState	Retrieves all ZIP codes in a state.
     * 
     */
    
    public function locationInfo($function, $state, $city=null)
    {
        $url = $this->url."?apikey={$this->api_key}&library=LocationInfo&function=$function&state=".urlencode($state)."&city=".urlencode($city);
        if ($zip) $url .= "&zip=".urlencode($zip);
        if ($address) $url .= "&address=".urlencode($address);
        if ($lat && $lon) { $url .= '&lat='.urlencode($lat).'&lon'.urlencode($lon); }
        $url .= '&minimumSchools='.urlencode($minimum).'&limit='.urlencode($limit);
        //ldd($url);
        $xml = $this->receive($url);
        return $xml;
    }
    
    public function receive($url)
    {
        $xml = simplexml_load_file($url);
        return $xml;
    }
}