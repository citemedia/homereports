<?php
namespace Api\MelissaData;
//https://property.melissadata.net/v3/REST/Service.svc/doLookup?id=106897455&t=RestTest&addressKey=92688211282&opt=1
//https://addresscheck.melissadata.net/v2/REST/Service.svc/doAddressCheck?id=106897455&opt=true&a1=22382%20Avenida%20%20Empresa&city=Rancho%20Santa%20Margarita&state=CA&zip=92688
class MelissaData
{
    private $customerId = 106897455;
    
    public function getFromZip($zip)
    {
        $url = "https://zipsearch.melissadata.net/v2/REST/Service.svc/doZipSearch?id={$this->customerId}&zip=".urlencode($zip);

        return simplexml_load_string(file_get_contents($url));
    }

    private function getAddressKey($property){
        $url = "https://addresscheck.melissadata.net/v2/REST/Service.svc/doAddressCheck?id={$this->customerId}&t=ndp&a1=".urlencode($property->street)."&city=".urlencode($property->city)."&state=".urlencode($property->state)."&zip=".urlencode($property->zipcode);
        var_dump( $url );die(); 
        return simplexml_load_string(file_get_contents($url));
    }

    public function getProperty($property){
        $address = $this->getAddressKey($property);
        if (!isset($address->Record->Address->AddressKey)) return false;
        
        $addressKey = $address->Record->Address->AddressKey;

        $url = "https://property.melissadata.net/v3/REST/Service.svc/doLookup/?id={$this->customerId}&t=ndp&addressKey=".urlencode($addressKey)."&opt=1";

        return simplexml_load_string(file_get_contents($url));
    }
    
}