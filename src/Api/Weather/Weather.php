<?php
namespace Api\Weather;

class Weather
{
    private $url = "http://api.wunderground.com/api";
    private $key = '0150dcd5edeb0b89';
    
    private $state;
    private $city;

    public function __construct($state, $city) {
        $this->state    = urlencode($state);
        $this->city     = urlencode($this->prepareCity($city));

    }

    public function results($url)
    {
        $countFile = __DIR__.'/../../../cache/counter';
        $time = date('Ymdhi');

        if (!is_file($countFile)) {
            $counter = [];
            $counter[$time] = 0;
            file_put_contents($countFile, serialize($counter));
        }

        $counter = unserialize(file_get_contents($countFile));

        if (!isset($counter[$time])) {
            $counter = [];
            $counter[$time] = 0;
        } 
        
        $counter[$time]++;
        
        file_put_contents($countFile, serialize($counter));

        if ($counter[$time]<10) {
            return json_decode(file_get_contents($url));
        }

        return false;
    }

    public function getLookup() {
        $url = $this->url.'/'.$this->key.'/geolookup/q/'.$this->state.'/'.$this->city.'.json';
        return $this->results($url);
    }

    public function getConditions() {
        $url = $this->url.'/'.$this->key.'/conditions/q/'.$this->state.'/'.$this->city.'.json';
        return $this->results($url);
    }

    public function getForecast() {
        $url = $this->url.'/'.$this->key.'/forecast/q/'.$this->state.'/'.$this->city.'.json';
        return $this->results($url);
    }
    
    public function getAstronomy() {
        $url = $this->url.'/'.$this->key.'/astronomy/q/'.$this->state.'/'.$this->city.'.json';
        return $this->results($url);
    }

    public function getAlmanac() {
        $url = $this->url.'/'.$this->key.'/almanac/q/'.$this->state.'/'.$this->city.'.json';
        return $this->results($url);
    }

    private function prepareCity($city) {
        return str_replace(array(' '), array('_'), $city);
    }
}