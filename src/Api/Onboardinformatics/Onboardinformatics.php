<?php

namespace Api\Onboardinformatics;

use Silex\Application;

class Onboardinformatics
{

    //private $qa = 'http://api.qa.obiwebservices.com';
    //private $staging = 'http://api.staging.obiwebservices.com';
    private $url = 'http://api.obiwebservices.com';
    private $aid = '1007-e2e6c45c00e';
    public $atoken = '';

    function __construct(Application $app)
    {
        $this->app = $app;
    }

    function avm($params)
    {
        $get_string = http_build_query($params);
        $url = "http://xml.onboardservices.net/AVM/index.aspx?AID={$this->aid}&$get_string";
        //$url = "http://xml.onboardservices.net/AVM/index.aspx?AID=1007-e2e6c45c00e&street=First+avenue&city=Seattle&state=WA&zip=98121";
        $resp = file_get_contents($url);
        $xml = simplexml_load_string($resp);
        $ns = $xml->getNamespaces(true);
        return $xml->children($ns['XAVM'])->RESULT;
    }

    /**
     * $package variants:
     * Boundary/Detail
     * CBSA/Lookup
     * County/Lookup
     * Hierarchy/Lookup
     * State/Lookup
     */
    function getArea($package, $params)
    {
        $params['mime'] = 'json';
        $get_string = http_build_query($params);
        $url = $this->url . "/Area/{$package}/?AccessToken={$this->getSecurityToken()}&$get_string";
        $obj = json_decode(file_get_contents($url))->response;
        if ($obj->status->code == '0') {
            return $obj->result->package->item;
        } else {
            return false;
        }
    }

    function getCommunityAreaFull($params)
    {
        $params['mime'] = 'json';
        $get_string = http_build_query($params);
        $url = $this->url . "/Community/Area/Full?AccessToken={$this->getSecurityToken()}&$get_string";
        $obj = json_decode(file_get_contents($url))->response;
        if ($obj->status->code == '0') {
            return $obj->result->package->item[0];
        } else {
            return false;
        }
    }
    
    function getPropertySearch($params)
    {
        $params['mime'] = 'json';
        $get_string = http_build_query($params);
        $url = $this->url . "/Property+Search/Trend/Home+Sales?AccessToken={$this->getSecurityToken()}&$get_string";
        $obj = json_decode(file_get_contents($url))->response;
        if ($obj->status->code == '0') {
            return $obj->result->package->item;
        } else {
            return false;
        }
    }

//  function getAreaId($params)
//  {
//    $params['mime'] = 'json';
//    $get_string = http_build_query($params);
//    $url = $this->url . "/Area/Hierarchy/Lookup?AccessToken={$this->getSecurityToken()}&$get_string";
//    $obj = json_decode(file_get_contents($url))->response;
//    if ($obj->status->code == '0') {
//      //var_dump($obj->result->package->item[0]);die();
//      $this->atoken = $obj->result->package->item[0];
//
//      return $this->atoken;
//    } else {
//      return false;
//    }
//  }

    function getGeocode($address)
    {
        $address = str_replace(" ", "+", $address);

        $json = json_decode(file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false"));

        return $json->results;
    }

    function getSecurityToken()
    {
        if ($this->atoken) {
            return $this->atoken;
        } else {
            $url = $this->url . "/Security/AccessToken/Standard/?AID={$this->aid}&Uid=propertystudy&UidType=1&Domain=propertystudy.org&mime=json";
            $obj = json_decode(file_get_contents($url));
            $response = $obj->response;
            if ($response->status->code == '0') {
                $this->atoken = $response->result->package->item[0]->access_token;
                return $this->atoken;
            } else {
                return false;
            }
        }
    }

}
