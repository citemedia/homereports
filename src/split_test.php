<?php

use CM\Form\SearchForm;
use Silex\Application;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use CM\Service\TriangleApi;

$app->match('/homevalue9', function (Request $request) use ($app) {
  return split_homepage($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'homevalue9']);
})->bind('homevalue9');

$app->match('/declined', function (Request $request) use ($app) {
  return declined($app, ['nextUrl' => 'report']);
})->bind('declined');

$app->match('/warning', function (Request $request) use ($app) {
  return warning($app, $request, ['nextUrl' => 'select-payment']);
})->bind('warning');

$app->match('/select-payment', function (Request $request) use ($app) {
  return select_payment($app, $request, ['nextUrl' => 'new-or-existing']);
})->bind('select-payment');

$app->match('/new-or-existing', function (Request $request) use ($app) {
  return new_or_existing($app, $request, ['nextUrl' => 'feedback']);
})->bind('new-or-existing');

$app->match('/feedback', function (Request $request) use ($app) {
  return feedback($app, $request, ['nextUrl' => 'checkout']);
})->bind('feedback');

function declined(Application $app, Array $options)
{
  $api = new TriangleApi($app);
  $data = $app['session']->get('checkout_data');
  if (!$app['session']->get('upsell_decline', false)) {
    $result = $api->simpleOrder2($data, $app['session']->get('searchString'));
    if ($result->State == 'Error') {
      // error
    } else {
      $app['session']->set('upsell_decline', true);
    }
  }

  if ($app['session']->get('funnel') == 'homevalue9') {
    return $app->redirect($app['url_generator']->generate('ebookspecialintro'));
  }
  return $app->redirect($options['nextUrl']);
}

function feedback(Application $app, Request $request, Array $options)
{
//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

  if ('POST' == $request->getMethod()) {
    return $app->redirect($options['nextUrl']);
  }

  return $app['twig']->render('split/feedback.html.twig', ['options' => $options]);
}

function new_or_existing(Application $app, Request $request, Array $options)
{
//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

  if ('POST' == $request->getMethod()) {
    return $app->redirect($options['nextUrl']);
  }

  $app['session']->set('cardtype', $request->get('cardTypeR'));

  return $app['twig']->render('split/new_or_existing.html.twig', ['options' => $options]);
}

function select_payment(Application $app, Request $request, Array $options)
{
//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

  if ('POST' == $request->getMethod()) {
    return $app->redirect($options['nextUrl']);
  }
  
  $app['session']->set('checkout_version', null);

  return $app['twig']->render('split/select_payment.html.twig', ['options' => $options]);
}

function warning(Application $app, Request $request, Array $options)
{
//  if (!$app['session']->get('searchData')) {
//    return redirectHome($app);
//  }

  if ('POST' == $request->getMethod()) {
    return $app->redirect($options['nextUrl']);
  }

  return $app['twig']->render('split/warning.html.twig', ['options' => $options]);
}

function split_homepage(Application $app, Request $request, Array $options)
{
  $session = $app['session'];

  $session->set('funnel', $options['funnel']);
  $session->set('property', null);
  $session->set('searchData', null);
  $session->set('searchString', null);
  $session->set('order', null);
  $session->set('upsellOrder', null);

  /** @var Form $form */
  $form = $app['form.factory']
      ->createBuilder(new SearchForm())
      ->getForm();

  if ('POST' == $request->getMethod()) {

    $form->submit($request);

    if ($form->isValid()) {
      $data = $form->getData();
      $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip'];

      $session->set('searchData', $data);
      $session->set('searchString', $searchString);

      return $app->redirect($options['nextUrl']);
    }
  }

  return $app['twig']->render(
          'split/homepage.html.twig', [
          'searchForm' => $form->createView(),
          'options' => $options
          ]
  );
}
