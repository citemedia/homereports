<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use CM\Service\UpsellBilling;

$app->match('/upsella', function (Request $request) use ($app) {
    return upsella($app, $request, ['nextUrl' => 'activation']);
})->bind('upsella');
;
$app->match('/upsella.1', function (Request $request) use ($app) {
    return upsella1($app, $request, ['nextUrl' => 'notavailable']);
})->bind('upsella1');

$app->match('/upsella.2', function (Request $request) use ($app) {
    return upsella2($app, $request, ['nextUrl' => 'activation']);
})->bind('upsella2');

$app->match('/notavailable', function (Request $request) use ($app) {
    return notavailable($app, $request, ['nextUrl' => 'report']);
})->bind('notavailable');

$app->match('/upsella-no-thanks', function (Request $request) use ($app) {
    $r = new RedirectResponse($app['url_generator']->generate('upsella'));
    return $r;
})->bind('upsella_no_thanks');

function upsella(Application $app, Request $request, $options)
{
    $db = new Splits\UpsellSplit\Database\UpsellSplit($app);

    if ('POST' == $request->getMethod()) {
        
        $amount = $request->get('price');

        $r = new RedirectResponse($app['url_generator']->generate($options['nextUrl']));

        $ub = new UpsellBilling($app);

        switch ($amount) {
        /* case '7.95': */
        /*     $ub->upsellaSimple($app['session']->get('checkout_data'), $amount, '105', '14'); */
        /*     $app['session']->set('upsella_purchased', $amount); */
        /*     break; */
        case '15.95':
            $ub->upsellaSimple($app['session']->get('checkout_data'), $amount, '106', '14');
            $app['session']->set('upsella_purchased', $amount);
            break;
        /* case '4.95': */
        /*     $ub->upsellaSubscription($app['session']->get('checkout_data'), '29', '42', '15'); */
        /*     $app['session']->set('upsella_purchased', '4.95'); */
        /*     break; */
        case '9.95':
            $ub->upsellaSubscription($app['session']->get('checkout_data'), '30', '43', '15');
            $app['session']->set('upsella_purchased', '9.95');
            break;
        default:
            var_dump('upsella error');die();
            break;
        }

        return $r;
    }

    $html = $app['twig']->render('upsell_split/upsella.html.twig', ['options' => $options]);
    $r = new Response($html);

    return $r;
}

function upsella1(Application $app, Request $request, $options)
{
    $app['session']->set('referring_page', 'upsellA.1');

    $db = new Splits\UpsellSplit\Database\UpsellSplit($app);

    if ('POST' == $request->getMethod()) {

        $r = new RedirectResponse($app['url_generator']->generate($options['nextUrl']));
        $db->updateVisitor($r, 'upsellA.1', 'upsellA.1', 'yes', $request->get('price'));
        return $r;
    }

    $html = $app['twig']->render('upsell_split/upsella1.html.twig', ['options' => $options]);
    $r = new Response($html);
    $db->updateVisitor($r, 'upsellA.1', 'upsellA.1', 'no', null);

    return $r;
}

function upsella2(Application $app, Request $request, $options)
{
    $app['session']->set('referring_page', 'upsellA.2');

    $db = new Splits\UpsellSplit\Database\UpsellSplit($app);

    if ('POST' == $request->getMethod()) {
    
        $amount = $request->get('price');
        $r = new RedirectResponse($app['url_generator']->generate($options['nextUrl']));
        //$db->updateVisitor($r, 'upsellA.2', 'upsellA.2', 'yes', $amount);
        $ub = new UpsellBilling($app);

        switch ($amount) {
        /* case '11.95': */
        /*     $ub->upsellaSimple($app['session']->get('checkout_data'), $amount, '103', '14'); */
        /*     $app['session']->set('upsella_purchased', $amount); */
        /*     break; */
        case '19.95':
            $ub->upsellaSimple($app['session']->get('checkout_data'), $amount, '104', '14');
            $app['session']->set('upsella_purchased', $amount);
            break;
        /* case '8.95': */
        /*     $ub->upsellaSubscription($app['session']->get('checkout_data'), '27', '40', '15'); */
        /*     $app['session']->set('upsella_purchased', '8.95'); */
        /*     break; */
        case '13.95':
            $ub->upsellaSubscription($app['session']->get('checkout_data'), '28', '41', '15');
            $app['session']->set('upsella_purchased', '13.95');
            break;
        default:
            var_dump('upsella2 error');die();
            break;
        }

        return $r;
    }

    $html = $app['twig']->render('upsell_split/upsella2.html.twig', ['options' => $options]);
    $r = new Response($html);
    //$db->updateVisitor($r, 'upsellA.2', 'upsellA.2', 'no', null);

    return $r;
}

function notavailable(Application $app, Request $request, $options)
{
    return $app['twig']->render('upsell_split/notavailable.html.twig', ['options' => $options]);
}
