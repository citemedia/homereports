<?php

namespace Subscribers;

use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Silex\Application;

class AuthenticationListener implements EventSubscriberInterface
{
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function onAuthenticationSuccess(AuthenticationEvent $event)
    {
        $username = $event->getAuthenticationToken()->getUser()->getUsername();
        $provider = new \Database\UserProvider($this->app['db']);

        try {
            $provider->lastLoginUser($username);
        } catch (\Exception $e) {} 
    }

    public static function getSubscribedEvents()
    {
        return array(
            AuthenticationEvents::AUTHENTICATION_SUCCESS => array(array('onAuthenticationSuccess', 32)) 
        );
    }
}
