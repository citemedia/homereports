<?php

use Assetic\Filter\CssRewriteFilter;
use Assetic\Filter\GoogleClosure\CompilerJarFilter;
use Assetic\Filter\Yui\CssCompressorFilter;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\FormServiceProvider;
use SilexAssetic\AsseticServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use CM\Form\LoginForm;

define('ROOT', __DIR__ . '/..');

require_once ROOT . "/vendor/autoload.php";

$app = new Silex\Application();

// turn on debug
if (php_sapi_name() === 'cli' || $_SERVER['REMOTE_ADDR'] === '127.0.0.1') {
    $app['dev'] = true;
    $app['debug'] = true;
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    $logfile = ROOT . "/logs/dev.log";
    $ladybug = new \Ladybug\Dumper();
    $ladybug->setTheme('modern');
} else {
    $app['dev'] = false;
    $app['debug'] = false;
    $logfile = ROOT . "/logs/prod.log";
}
//$app['dev'] = false;$app['debug'] = false;

$app->register(new Silex\Provider\SessionServiceProvider(), array(
    'session.test' => php_sapi_name() === 'cli'
));

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => array(ROOT . "/src/views"),
    'twig.options' => array(
        'cache' => ROOT . "/cache/twig",
        'auto_reload' => true,
        'strict_variables' => false)));

$app["twig"] = $app->share($app->extend("twig", function (\Twig_Environment $twig, Silex\Application $app) {
            $twig->addExtension(new CM\Twig\Extension\TwigExtension($app));

            return $twig;
        }));

$app['assetic.path_to_web'] = ROOT . '/web';
$app->register(new AsseticServiceProvider());

$app['assetic.filter_manager']->set('yui_css', new CssCompressorFilter(ROOT . "/bin/jar/yuicompressor.jar"));
$app['assetic.filter_manager']->set('closure', new CompilerJarFilter(ROOT . "/bin/jar/compiler.jar"));
$app['assetic.filter_manager']->set('cssrewrite', new CssRewriteFilter());

$app['assetic.options'] = [
    'auto_dump_assets' => false,
//    'auto_dump_assets' => true,
    'debug' => $app['dev']
];

$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

$app->register(new UrlGeneratorServiceProvider);

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => $logfile,
    'monolog.name' => 'silex',
    'monolog.logger.class' => 'CM\CMLogger'
));

$app['monolog.level'] = function () {
    return \Monolog\Logger::ERROR;
};

//$app['swiftmailer.options'] = array(
//    'host' => 'host',
//    'port' => '25',
//    'username' => 'username',
//    'password' => 'password',
//    'encryption' => null,
//    'auth_mode' => null
//);
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app->register(new Config\ConfigProvider(ROOT . '/config/config.yaml'));

require 'main.php';

$app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => $app['config']['database']
));
$app['security.firewalls'] = array(
    'subscribed' => array(
        'anonymous' => true,
        'pattern' => '^.*$',
        'form' => array(
            'login_path' => '/login',
            'check_path' => '/login-check',
            'username_parameter' => LoginForm::NAME . '[email]',
            'password_parameter' => LoginForm::NAME . '[password]',
            'default_target_path' => 'homepage1'),
        'logout' => array('logout_path' => '/logout'),
        'users' => $app->share(function () use ($app) {
            return new \Database\UserProvider($app['db']);
        }),
    ),
);
$app->register(new Silex\Provider\SecurityServiceProvider(), array());
$app['security.encoder.digest'] = $app->share(function ($app) {
    return new \Database\RawPasswordEncoder();
});

$app['dispatcher']->addSubscriber(new Subscribers\AuthenticationListener($app));

// intercept json request
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

return $app;

