var message = "Are you sure you want to leave?\n-------------------------------------------------------------------------------\nStay on this page to receive your property record report! Join the thousand's of customer's using these services every day! Let us help you make the right decision when comes to buying and selling real estate.\n-------------------------------------------------------------------------------";
var vals = 0;
var t = null;
var doredirect = true;
$(window).bind('beforeunload', function(ev) {
    // For IE and Firefox
    if (ev) {
      ev.returnValue = message;
    }
    if (/Firefox[\/\s](\d+)/.test(navigator.userAgent) && new Number(RegExp.$1) >= 4) {
        if (confirm(message)) {
            doredirect = false;
        } else {
            doredirect = true;
            timed();
            ev.returnValue = message;
            return message;
        }
    } else {
        doredirect = true;
        timed();
        return message;
    }
});

function timed() {
    t = setTimeout("timed()", 100);
    if (vals > 0)
    {
        clearTimeout(t);
        leavepage();
        to_discount();
    } else {
        vals++;
    }
}

function leavepage() {
    $.post("/leave", {});
}

function to_discount() {
    $(window).unbind('beforeunload');
    $.post("/to-discount", {});
    if (doredirect) {
        window.location.replace("{{ app.request.getSchemeAndHttpHost() ~ '/checkout-discount' }}");
    }
}

$(document).ready(function() {
    $('.unbind_unload').click(function(ev) {
        $(window).unbind('beforeunload');
    });
});