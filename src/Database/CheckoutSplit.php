<?php

namespace Database;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class CheckoutSplit
{
    
    public $app;
    public $conn;
    public $tablename = 'checkout_split';
    public $cookie_name = 'checkout_split';

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
        $this->conn = $app['db'];
    }
    
    private function updateRow($page, $bought, $price, $cookie_id)
    {
        $date = new \DateTime();
        $date->setTimestamp(time());
        $last_visit = $date->format('Y-m-d H:i:s');
        $tablename = $this->tablename;
        $ip = $this->app['request']->getClientIp();
        
        $sql = <<<SQL
INSERT INTO {$tablename} (page, bought, price, cookie_id, last_visit, ip)
VALUES ('{$page}', '{$bought}', '{$price}', '{$cookie_id}', '{$last_visit}', '$ip')
ON DUPLICATE KEY UPDATE page='{$page}', bought='{$bought}', price='{$price}', cookie_id='{$cookie_id}', last_visit='{$last_visit}', ip='{$ip}';
SQL;
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return $cookie_id;
    }

    private function createTable()
    {
        $tablename = $this->tablename;
        
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `{$tablename}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(63) NOT NULL,
  `bought` varchar(3) DEFAULT 'no',
  `price` varchar(8) DEFAULT NULL,
  `cookie_id` varchar(23) NOT NULL,
  `last_visit` datetime NOT NULL,
  `ip` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cookie_id` (`cookie_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
SQL;
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
    
    private function handleErrors($msg)
    {
        //SQLSTATE error codes
        if (strripos($msg, '42s02') !== false) {
            // table not found
            $this->createTable();
        }
        if (strripos($msg, '42000') !== false) {
            // syntax error or access rule violation
            // incorrect /config/config.yaml
        }
    }
    
    public function updateVisitor(Response $response, $page, $bought, $price)
    {
        $cookie_id = $this->app['request']->cookies->get($this->cookie_name);
        if (!$cookie_id) {
            $cookie_id = uniqid();
        }
        //$cookie_id = uniqid();
        $this->updateRow($page, $bought, $price, $cookie_id);
        $expire = new \DateTime();
        $expire->setTimestamp(time() + 2764800); // current timestamp + 32 days in seconds 
        $cookie = new Cookie($this->cookie_name, $cookie_id, $expire);
        $response->headers->setCookie($cookie);
        
        return $response;
    }

}