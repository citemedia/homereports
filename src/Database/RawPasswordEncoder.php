<?php

namespace Database;

use \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class RawPasswordEncoder implements PasswordEncoderInterface
{
    /**
     * {@inheritdoc}
     */
    public function encodePassword($raw, $salt)
    {
        return $raw;
    }

    /**
     * {@inheritdoc}
     */
    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded == $raw;
    }
}