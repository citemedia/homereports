<?php

use Symfony\Component\HttpFoundation\Request;
use CM\Form\SearchPasswordForm;

global $app;

$app->match('/search-password', function (Request $request) use ($app) {
    return search_passwords($app, $request);
})->bind('search-password')->requireHttps();

function search_passwords($app, $request)
{
    $support = ($app['security']->isGranted('ROLE_SUPPORT') || $app['security']->isGranted('ROLE_ADMIN'));
    if (!$support) {
        return $app->redirect($app['url_generator']->generate('login'));
    }
    
    $form = $app['form.factory']
            ->createBuilder(new SearchPasswordForm())
            ->getForm();
    
    $error = '';
    $password= '';
    if ('POST' == $request->getMethod()) {
        $password= 'Username/email not found.';
        $form->submit($request);
        $data = $form->getData();
        $sql = "SELECT * FROM users WHERE (`roles`='ROLE_USER' AND `username` = '{$data['email']}') LIMIT 1";
        $row = $app['db']->fetchAssoc($sql);
        if (!empty($row)) {
            $password = "Password:  {$row['password']}";
        }
    }
    
    return $app['twig']->render('support/support.twig', [
        'form' => $form->createView(),
        'error' => $error,
        'password' => $password
    ]);
}
