<?php

use CM\Form\LoginForm;
use CM\Form\ForgotForm;
use CM\Form\MortgageForm;
use CM\Form\ReportMissingInfoForm;
use CM\Form\OrderForm;
use CM\Form\OrderForm2;
use CM\Form\SearchForm;
use CM\Form\TopSearchForm;
use CM\Service\TriangleApi;
use Silex\Application;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Api\ZillowApi\ZillowApi;
use Api\MelissaData\MelissaData;
use Api\GreatSchools\GreatSchoolsAPI;
use Api\PredatorBarrier\PredatorBarrier;
use Api\WalkScore\WalkScore;
use Api\Weather\Weather;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

require "admin.php";
require "split_test.php";
require "ebookspecial.php";
require "checkout_split.php";
require "report_steps.php";
require "upsell_split3.php";
require "userstats.php";

$app->match('/{url}', function(Request $request) use ($app) {
    $pathInfo = $request->getPathInfo();
    $requestUri = $request->getRequestUri();

    $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

    return $app->redirect($url, 301);
})->bind('removeTrailingSlash')->assert('url', '.*/$');

$app->post('/log-error', function (Request $request) use ($app) {
    $rootUrl = $app['url_generator']->generate('homepage1', array(), UrlGenerator::NETWORK_PATH);

    if ($request->get('msg') && stristr($request->get('url'), $rootUrl)) {
        $app['monolog']->crit('JS ERROR: ' . $request->get('msg'), [
            'extra' => [
                'line' => $request->get('line'),
                'file' => $request->get('url')
            ]
        ]);
    }
    return new Response();
})->bind('log-error');

$app->match('/security-homebook', function(Request $request) use ($app) {
    $size = filesize("../web/ebook/HomeSecurityEbook.pdf");

    return $app->stream(function() {
            readfile("../web/ebook/HomeSecurityEbook.pdf");
        }, 200, array(
            'Content-Type' => 'application/pdf',
            'Content-length' => $size,
            'Content-Disposition' => 'attachment; filename="HomeSecurityEbook.pdf"',
            'Content-Transfer-Encoding' => 'binary'
    ));
})->bind('security-homebook');

$app->match('/notavailable_a', function (Request $request) use ($app) {
    return $app['twig']->render('id_upsell/notavailable_a.html.twig');
})->bind('notavailable_a');

$app->get('/searching', function (Request $request) use ($app) {
    return searching($app, $request, ['nextUrl' => 'requesting']);
})->bind('searching');

$app->get('/requesting', function (Request $request) use ($app) {
    return requesting($app, $request, ['nextUrl' => 'download']);
})->bind('requesting');

$app->match('/download', function (Request $request) use ($app) {
    return download($app, $request, ['nextUrl' => 'finished']);
})->bind('download');

$app->get('/finished', function (Request $request) use ($app) {
    return finished($app, $request, ['nextUrl' => 'checkout']);
})->bind('finished');

// end additional "finished" pages
// cost

$app->match('/cost', function (Request $request) use ($app) {
    return cost($app, $request, ['nextUrl' => 'checkout', 'type' => 'cost']);
})->bind('cost');

$app->get('/costdiscount', function (Request $request) use ($app) {
    return cost($app, $request, ['nextUrl' => 'checkout', 'type' => 'costdiscount']);
})->bind('costdiscount');

$app->get('/costpromo', function (Request $request) use ($app) {
    return cost($app, $request, ['nextUrl' => 'checkout', 'type' => 'promo']);
})->bind('costpromo');

// end cost

$app->match('/checkout', function (Request $request) use ($app) {
    return checkout($app, $request, ['nextUrl' => 'specialoffer']);
})->bind('checkout')->requireHttps();
$app->match('/checkout3', function (Request $request) use ($app) {
    return checkout3($app, $request, ['nextUrl' => 'specialoffer']);
})->bind('checkout3')->requireHttps();


$app->match('/specialoffer', function (Request $request) use ($app) {
    return upsell($app, $request, ['template' => 'preupsell2.html.twig', 'form' => 'CM\Form\PreUpsellForm2'], 'crm_upsell2_product_id');
})->bind('specialoffer');

$app->match('/specialoffer2', function (Request $request) use ($app) {
    return upsell($app, $request, ['template' => 'upsell.html.twig', 'form' => 'CM\Form\UpsellForm']);
})->bind('specialoffer2');

$app->match('/activation', function (Request $request) use ($app) {
    return activation($app, $request);
})->bind('activation');

/* $app->get('/report', function (Request $request) use ($app) { */
/*     return reports($app, $request); */
/* })->bind('report'); */

$app->post('/mortgage', function (Request $request) use ($app) {
    return mortgage($app, $request);
})->bind('mortgage');

$app->match('/customer-service', function (Request $request) use ($app) {
    return $app['twig']->render('customer_service.html.twig');
})->bind('customer-service');

$app->match('/faq', function (Request $request) use ($app) {
    return $app['twig']->render('faq.html.twig');
})->bind('faq');

$app->match('/terms', function (Request $request) use ($app) {
    return $app['twig']->render('terms.html.twig');
})->bind('terms');

$app->match('/privacy', function (Request $request) use ($app) {
    return $app['twig']->render('privacy.html.twig');
})->bind('privacy');

//*****************FUNNEL 1****************************************************************************
$app->match('/', function (Request $request) use ($app) {
    if ($app['security']->isGranted('ROLE_USER')) {
        $url = 'report';
    } else {
        $url = 'searching1';
    }
    return homepage($app, $request, ['nextUrl' => $url, 'index' => false, 'funnel' => 'root']);
})->bind('homepage1');

$app->get('/searching1', function (Request $request) use ($app) {
    return searching($app, $request, ['nextUrl' => 'requesting1']);
})->bind('searching1');

$app->get('/requesting1', function (Request $request) use ($app) {
    return requesting($app, $request, ['nextUrl' => 'download1']);
})->bind('requesting1');

$app->get('/download1', function (Request $request) use ($app) {
    return download($app, $request, ['nextUrl' => 'finished1']);
})->bind('download1');

$app->get('/finished1', function (Request $request) use ($app) {
    return finished($app, $request, ['nextUrl' => 'checkout1']);
})->bind('finished1');

$app->match('/checkout1', function (Request $request) use ($app) {
    return checkout($app, $request, ['nextUrl' => 'bonusoffer']);
})->bind('checkout1')->requireHttps();

$app->match('/bonusoffer', function (Request $request) use ($app) {
    return upsell($app, $request, ['template' => 'preupsell.html.twig', 'form' => 'CM\Form\Upsell2Form']);
})->bind('bonusoffer');

$app->match('/login', function (Request $request) use ($app) {
    return login($app, $request);
})->bind('login');

//$app->match('/login-check', function (Request $request) use ($app) {
//    return new Response();
//})->bind('login-check');

$app->match('/forgot-password', function (Request $request) use ($app) {
    return forgot($app, $request);
})->bind('forgot');

$app->get('/logout', function (Request $request) use ($app) {
    return logout($app, $request);
})->bind('logout');

$app->post('/report-missing-info', function (Request $request) use ($app) {
    return report_missing_info($app, $request);
})->bind('report-missing-info');

$app->post('/to-discount', function (Request $request) use ($app) {
    return to_discount($app, $request);
})->bind('to_discount');

$app->match('/checkout-discount', function (Request $request) use ($app) {
    return checkout_discount($app, $request, [
        'nextUrl' => 'upsella2'
    ]);
})->bind('checkout_discount');

function to_discount($app, $request)
{
    $app['session']->set('to_discount', true);
    return new Response();
}

function checkout_discount($app, $request, $options)
{
    $form = $app['form.factory']
        ->createBuilder(new OrderForm())
        ->getForm();

    $app['session']->set('checkout_discount', true);

    if ('POST' == $request->getMethod()) {
        $form->submit($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $app['session']->set('checkout_data', $data);

            $api = new TriangleApi($app);
            // if successful, sets 'prospectID' session var
            $api->setProspectID($data);
            $result = $api->simpleOrderCheckoutDiscount($data, $app['session']->get('searchString'));

            if ($result->State == 'Error') {
                return $app['twig']->render('checkout_discount.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }

            $app['session']->set('to_discount', false);
            $app['session']->set('customerData', $data);
            $app['session']->set('order', $result); 

            return $app->redirect($app['url_generator']->generate($options['nextUrl']));
        }
    }

    return $app['twig']->render('checkout_discount.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
}

function redirectHome(Application $app)
{
    $session = $app['session'];

    if ($funnel = $session->get('funnel')) {
        if (($funnel == 'propertyrecord77') || ($funnel == 'propertyrecord77_10')) {
            return $app->redirect($app['url_generator']->generate('homepage'));
        } else {
            return $app->redirect($app['url_generator']->generate('homepage1'));
        }
    } else {
        return $app->redirect($app['url_generator']->generate('homepage1'));
    }
}

function login($app, $request)
{
    $form = $app['form.factory']
        ->createBuilder(new LoginForm())
        ->getForm();

    // get the login error if there is one
    if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
        $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
    } else {
        $error = $app['session']->get(SecurityContext::AUTHENTICATION_ERROR);
        $app['session']->remove(SecurityContext::AUTHENTICATION_ERROR);
    }
    if ($error) {
        $error = $error->getMessage();
    }

    return $app['twig']->render('login.html.twig', array(
            'form' => $form->createView(),
            'error' => $error,
            'last_username' => $app['session']->get(SecurityContext::LAST_USERNAME),
    ));
}

function forgot($app, $request)
{
    if ($app['security']->isGranted('ROLE_USER') && !($app['security']->isGranted('ROLE_ADMIN') || $app['security']->isGranted('ROLE_SUPPORT'))) {
        return $app->redirect($app['url_generator']->generate('homepage1'));
    }

    /** @var Form $form */
    $form = $app['form.factory']
        ->createBuilder(new ForgotForm())
        ->getForm();

    $error = '';

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $support = false;
            $to_email = $data['email'];
            if ($app['security']->isGranted('ROLE_ADMIN') || $app['security']->isGranted('ROLE_SUPPORT')) {
                $support = true;
                $to_email = $app['config']['site_email'];
            }

            try {
                $found_user = $app['security.user_provider.subscribed']->loadUserByUsername($data['email']);
                // Send email to the user:
                $message = \Swift_Message::newInstance()
                    ->setSubject('homereports.org forgot password.')
                    ->setFrom(array($app['config']['site_email']))
                    ->setTo(array($to_email));
                if ($support) {
                    $message->setBody("homereports.org<br/><br/>username: {$found_user->getUsername()}<br/>password: " . $found_user->getPassword(), 'text/html');
                } else {
                    $message->setBody('homereports.org<br/><br/>password: ' . $found_user->getPassword(), 'text/html');
                }

                $app['mailer']->send($message);

                return $app->redirect($app['url_generator']->generate('homepage1'));
            } catch (UsernameNotFoundException $ex) {
                return $app['twig']->render('forgot.html.twig', [
                        'form' => $form->createView(),
                        'error' => 'Email not found.'
                ]);
            } catch (\Swift_RfcComplianceException $e) {
                $error = 'Invalid email address.';
            }
        }
    }

    return $app['twig']->render(
            'forgot.html.twig', [
            'form' => $form->createView(),
            'error' => $error
            ]
    );
}

function homepage(Application $app, Request $request, Array $options)
{
    $session = $app['session'];

    $app['session']->set('to_discount', false);

    $session->set('funnel', $options['funnel']);
    $session->set('property', null);
    $session->set('searchData', null);
    $session->set('searchString', null);
    $session->set('order', null);
    $session->set('upsellOrder', null);

    $form = $app['form.factory']
        ->createBuilder(new SearchForm())
        ->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip'];

            $session->set('searchData', $data);
            $session->set('searchString', $searchString);

            return $app->redirect($options['nextUrl']);
        }
    }

    /* return $app['twig']->render( */
    /*         'homepage.html.twig', [ */
    /*         'searchForm' => $form->createView(), */
    /*         'options' => $options */
    /*         ] */
    /* ); */
    return $app['twig']->render(
            'cs_homepage.html.twig', [
            'searchForm' => $form->createView(),
            'options' => $options
            ]
    );
}

function searching(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ($app['session']->get('funnel') == 'homevalue9') {
        return $app['twig']->render('split/searching.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('searching.html.twig', ['options' => $options]);
    }
}

function requesting(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ($app['session']->get('funnel') == 'homevalue9') {
        return $app['twig']->render('split/requesting_to_download.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('requesting_to_download.html.twig', ['options' => $options]);
    }
}

function download(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ($app['session']->get('funnel') == 'homevalue9') {
        return $app['twig']->render('split/download.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('download.html.twig', ['options' => $options]);
    }
}

function finished(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ($app['session']->get('funnel') == 'homevalue9') {
        $options['nextUrl'] = 'cost';
        return $app['twig']->render('split/finished_downloading.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('finished_downloading.html.twig', ['options' => $options]);
    }
}

// cost testing code

function cost(Application $app, Request $request, Array $options)
{
    if ($app['security']->isGranted('ROLE_USER')) {
        $app['session']->set('order', true);
        return $app->redirect('report');
    }

    if ('POST' == $request->getMethod() && $app['session']->get('funnel') == 'homevalue9') {
        return $app->redirect('/warning');
    }

    $searchData = $app['session']->get('searchData');

    if ($app['session']->get('funnel') == 'homevalue9') {
        $options['nextUrl'] = 'warning';
        return $app['twig']->render('split/cost_of_report.html.twig', ['options' => $options, 'zip' => $searchData['zip']]);
    } else {
        return $app['twig']->render('cost_of_report.html.twig', ['options' => $options, 'zip' => $searchData['zip']]);
    }
}

function checkout3(Application $app, Request $request, Array $options)
{
    $form = $app['form.factory']
        ->createBuilder(new OrderForm2())
        ->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $app['session']->set('checkout_data', $data);

            $api = new TriangleApi($app);
            // if successful, sets 'prospectID' session var
            $api->setProspectID($data);
            $result = $api->simpleOrder1($data, $app['session']->get('searchString'));

            if ($result->State == 'Error') {
                return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }

            $app['session']->set('upsell_decline', false);
            $app['session']->set('customerData', $data);
            $app['session']->set('order', $result);

            return $app->redirect($options['nextUrl']);
        }
    }

    return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
}

function checkout(Application $app, Request $request, Array $options)
{
    if ($app['security']->isGranted('ROLE_USER')) {
        $app['session']->set('order', true);
        return $app->redirect('report');
    }

    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ($app['session']->get('order')) {
        return $app->redirect($options['nextUrl']);
    }

    $app['session']->set('checkout_discount', null);

    if ($app['session']->get('funnel') == 'homevalue9') {
        $form = $app['form.factory']
            ->createBuilder(new OrderForm2())
            ->getForm();

        if ('POST' == $request->getMethod()) {

            $form->submit($request);

            if ($form->isValid()) {

                $data = $form->getData();
                $app['session']->set('checkout_data', $data);

                $api = new TriangleApi($app);
                // if successful, sets 'prospectID' session var
                $api->setProspectID($data);
                $result = $api->simpleOrder1($data, $app['session']->get('searchString'));

                if ($result->State == 'Error') {
                    return $app['twig']->render('checkout.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
                }

                $app['session']->set('upsell_decline', false);
                $app['session']->set('customerData', $data);
                $app['session']->set('order', $result);

                //return $app->redirect($options['nextUrl']);
                return $app->redirect('upsella.2');
            }
        }

        return $app['twig']->render('checkout.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
    } else {

        $form = $app['form.factory']
            ->createBuilder(new OrderForm2())
            ->getForm();

        if ('POST' == $request->getMethod()) {

            $form->submit($request);

            if ($form->isValid()) {

                $data = $form->getData();
                $app['session']->set('checkout_data', $data);

                $api = new TriangleApi($app);
                // if successful, sets 'prospectID' session var
                $api->setProspectID($data);
                $result = $api->simpleOrder($data, $app['session']->get('searchString'));

                if ($result->State == 'Error') {
                    return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
                }

                $app['session']->set('customerData', $data);
                $app['session']->set('order', $result);

                return $app->redirect($options['nextUrl']);
            } else {
                return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }
        }

        return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
    }
}

function upsell(Application $app, Request $request, Array $options, $productId = 'crm_upsell_product_id')
{
    if ($app['security']->isGranted('ROLE_USER')) {
        $app['session']->set('order', true);
        return $app->redirect('report');
    }

    if (!$app['session']->get('order')) {
        return $app->redirect('checkout');
    }

    if ($app['session']->get('upsellOrder')) {
        return $app->redirect('report');
    }

    $data = $app['session']->get('customerData');

    if ($data) {
        $form = $app['form.factory']
            ->createBuilder(new $options['form']($data['emailAddress']))
            ->getForm();

        if ('POST' == $request->getMethod()) {

            $form->submit($request);

            if ($form->isValid()) {

                $api = new TriangleApi($app);
                $data = array_merge($data, $form->getData());
                
                if (!$api->isCCDupe($data['ccNumber'])) {

                    $result = $api->subscribe($data, $app['session']->get('searchString'));

                    if ($result->State == 'Error') {
                        return $app['twig']->render($options['template'], [
                                    'upsell_form' => $form->createView()
                        ]);
                    }

                    $prospectId = $app['session']->get('prospectID');
                    $app['security.user_provider.subscribed']->createUser($data, $prospectId);
                    $app['session']->set('upsellOrder', $result);
                }
                
                return $app->redirect('activation');
            }
        }

        return $app['twig']->render($options['template'], ['upsell_form' => $form->createView()]);
    }

    return $app->redirect('report');
}

function activation(Application $app, Request $request)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    /* if (!$app['session']->get('upsellOrder')) { */
    /*     return redirectHome($app); */
    /* } */

    $next = $app['url_generator']->generate('report');

    return $app['twig']->render('activation.html.twig', ['next'=>$next]);
}

/* function reports(Application $app, Request $request) */
/* { */
/*     if (!$app['session']->get('searchData')) { */
/*         return redirectHome($app); */
/*     } */

/*     if (!$app['session']->get('order') && !$app['security']->isGranted('ROLE_USER')) { */
/*         return redirectHome($app); */
/*     } */

/*     $zillow = new ZillowApi($app['zwsid']); */
/*     $mode = $request->query->get('mode'); */

/*     $reportMissingInfoForm = $app['form.factory'] */
/*         ->createBuilder(new ReportMissingInfoForm()) */
/*         ->getForm(); */
/*     $session = $app['session']; */

/*     if ($app['security']->isGranted('ROLE_USER')) { */
/*         $topForm = $app['form.factory'] */
/*             ->createBuilder(new TopSearchForm()) */
/*             ->getForm() */
/*             ->setData($session->get('searchData')); */
/*         if ('POST' == $request->getMethod()) { */

/*             $topForm->submit($request); */

/*             if ($topForm->isValid()) { */
/*                 $data = $topForm->getData(); */
/*                 $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip']; */

/*                 $session->set('searchData', $data); */
/*                 $session->set('searchString', $searchString); */
/*             } */
/*             return $app->redirect('report'); */
/*         } */
/*         $topForm = $topForm->createView(); */
/*     } else { */
/*         $topForm = false; */
/*     } */

/*     //$mode = 'dev'; */
/*     if ($mode == 'dev') { */

/*         $app['street'] = '330 N 83rd St'; */
/*         $app['city'] = 'Seattle'; */
/*         $app['state'] = 'WA'; */
/*         $app['zip'] = ''; */

/*         $cityStateZip = urldecode($app['city'] . ' ' . $app['state']); */
/*         $ds = $zillow->GetDeepSearchResults(['address' => $app['street'], 'citystatezip' => $cityStateZip]); */

/*         if (isset($ds[0]) && isset($ds[0]->xpath('response/results')[0])) { */
/*             $property = json_decode(json_encode($ds[0]->xpath('response/results')[0]->result)); */
/*         } else { */
/*             $property = new stdClass(); */
/*         } */

/*         $property->zpid = 2129487543; */
/*         $property->finishedSqFt = 1; */
/*         $property->zestimate = array(); */
/*         $property->zestimate['last-updated'] = null; */
/*         $property->address = new stdClass(); */
/*         $property->address->street = 'street'; */
/*         $property->address->city = 'Seattle'; */
/*         $property->address->state = 'WA'; */
/*         $property->address->zipcode = '98921'; */
/*         $property->address->latitude = '47.67959'; */
/*         $property->address->longitude = '-122.35534'; */
/*         $prop_addr = array(); */
/*         $prop_addr['address'] = array(); */
/*         $prop_addr['address']['latitude'] = '47.67959'; */
/*         $prop_addr['address']['longitude'] = '-122.35534'; */
/*         $app['property'] = $prop_addr; */
/*         $session->set('searchString', '817 clarke avenue encinitas CA 92024'); */
/*     } else { */

/*         if (($session->get('funnel') == 'propertyrecord77') && !$session->get('order')) { */
/*             return $app->redirect('/checkout'); */
/*         } */

/*         $data = $session->get('searchData'); */
/*         $searchString = $session->get('searchString'); */

/*         $zillow = new ZillowApi($app['zwsid']); */
/*         $xml = $zillow->GetDeepSearchResults(['address' => $data['street'], 'citystatezip' => $searchString]); */

/*         if ($xml[0]->xpath('response/results')) { */
/*             $property = json_decode(json_encode($xml[0]->xpath('response/results')[0]->result)); */
/*         } else { */
/*             $property = null; */
/*         } */
/*         $session->set('property', $property); */
/*     } */

/*     if (is_object($property) && isset($property->zpid)) { */

/*         $greatschools_key = 'shmu5qtv08rquudifxsfggxx'; */
/*         $predatorbarrier_key = 'd53avvq3xt4oc8sskkkocggg'; */

/*         $gs = new GreatSchoolsAPI($greatschools_key); */
/*         $gs_result = $gs->nearbySchools($property->address->city, $property->address->state); */
/*         $app['nearbySchools'] = $gs_result; */

/*         $pb = new PredatorBarrier($predatorbarrier_key); */
/*         $pb_result = $pb->results($property->address->zipcode, 1); */
/*         $app['predatorBarrier'] = $pb_result; */

/*         $md = new MelissaData(); */
/*         $mproperty = parseMelissaData($md->getProperty($property->address)); */
/*         $app['melissaData'] = ['zip' => $md->getFromZip($property->address->zipcode), 'property' => $mproperty]; */

/*         $address = $property->address->street . ', ' . $property->address->zipcode . ', ' . $property->address->city . ', ' . $property->address->state; */
/*         $ws = new WalkScore(); */
/*         $app['walkScore'] = $ws->results($property->address->latitude, $property->address->longitude, $address); */

/*         $app['weather'] = new Weather($property->address->state, $property->address->city); */

/*         $app['property'] = $property; */
/*         $upd = $zillow->GetUpdatedPropertyDetails(['zpid' => $property->zpid]); */

/*         $images = null; */

/*         if ($upd->xpath('response')) { */
/*             $app['updatedProperties'] = json_decode(json_encode($upd->xpath('response')[0])); */

/*             if ($app['updatedProperties'] && isset($app['updatedProperties']->images)) { */
/*                 if ((int) $app['updatedProperties']->images->count) { */
/*                     $images = []; */
/*                     $cnt = 10; */
/*                     if (is_array($app['updatedProperties']->images->image->url) || is_object($app['updatedProperties']->images->image->url)) { */
/*                         foreach ($app['updatedProperties']->images->image->url as $image_src) { */
/*                             if ($cnt == 0) { */
/*                                 break; */
/*                             } */
/*                             $images[] = $image_src; */
/*                             $cnt--; */
/*                         } */
/*                     } else { */
/*                         $images[] = $app['updatedProperties']->images->image->url; */
/*                     } */
/*                 } */
/*             } */
/*         } */

/*         $zestimate = $zillow->GetZestimate(['zpid' => $property->zpid, 'rentzestimate' => true]); */
/*         if ($zestimate->xpath('response')) { */
/*             $app['zestimate'] = json_decode(json_encode($zestimate->xpath('response')[0])); */
/*         } */

/*         if (isset($app['zestimate'])) { */

/*             $mortgageXml = $zillow->GetMonthlyPayments([ */
/*                 'zpid' => $property->zpid, */
/*                 'price' => $app['zestimate']->zestimate->amount, */
/*                 'down' => 5, */
/*                 'zip' => $app['zestimate']->address->zipcode */
/*             ]); */

/*             $mortgage = null; */

/*             if ($mortgageXml->xpath('response')) { */
/*                 $mortgage = json_decode(json_encode($mortgageXml->xpath('response')[0])); */
/*             } */

/*             if ($mortgage) { */

/*                 $rates = [ */
/*                     '30 Years Fixed Rate ' . $mortgage->payment[0]->rate . '%', */
/*                     '15 Year Fixed Rate ' . $mortgage->payment[1]->rate . '%', */
/*                     '5/1 Adjustable Rate ' . $mortgage->payment[2]->rate . '%', */
/*                 ]; */
/*                 $app['mortgage'] = $mortgage; */
/*                 $app['mortgage']->payment = $mortgage->payment[0]; */

/*                 /\** @var Form $form *\/ */
/*                 $mortgageForm = $app['form.factory'] */
/*                     ->createBuilder(new MortgageForm($app['zestimate']->zestimate->amount, $rates, 5)) */
/*                     ->getForm(); */

/*                 return $app['twig']->render('reports.html.twig', ['mortgageForm' => $mortgageForm->createView(), */
/*                         'images' => $images, */
/*                         'reportMissingInfoForm' => $reportMissingInfoForm->createView(), */
/*                         'topForm' => $topForm, */
/*                         'firstName' => $session->get('customerData')['firstName'], */
/*                         'emailAddress' => $session->get('customerData')['emailAddress'] */
/*                 ]); */
/*             } */
/*         } */

/*         return $app['twig']->render('reports.html.twig', [ */
/*                 'images' => $images, */
/*                 'reportMissingInfoForm' => $reportMissingInfoForm->createView(), */
/*                 'topForm' => $topForm, */
/*                 'firstName' => $session->get('customerData')['firstName'], */
/*                 'emailAddress' => $session->get('customerData')['emailAddress'] */
/*         ]); */
/*     } else { */
/*         //sorry your report was not located, please search again or call customer service for a refund */
/*         $app['report_not_located'] = true; */
/*         return $app['twig']->render('reports.html.twig', [ */
/*                 'images' => '', */
/*                 'reportMissingInfoForm' => $reportMissingInfoForm->createView(), */
/*                 'topForm' => $topForm, */
/*                 'firstName' => $session->get('customerData')['firstName'], */
/*                 'emailAddress' => $session->get('customerData')['emailAddress'] */
/*         ]); */
/*     } */
/* } */

function parseMelissaData($data)
{
    if (!setlocale(LC_MONETARY, 'en_US')) {
        setlocale(LC_MONETARY, 'en_US.UTF-8');
    }
    foreach ((array) $data->Record->Values as $k => $v) {
        if (stristr($k, 'code') || stristr($k, 'year'))
            continue;
        $data->Record->Values->$k = str_replace('USD ', '$', money_format('%i', (double) $v));
    }
    $data->Record->CurrentSale->RecordingDate = date('Y/m/d', strtotime($data->Record->CurrentSale->RecordingDate));
    $data->Record->CurrentSale->SaleDate = date('Y/m/d', strtotime($data->Record->CurrentSale->SaleDate));
    $data->Record->CurrentSale->MortgageDate = date('Y/m/d', strtotime($data->Record->CurrentSale->MortgageDate));
    $data->Record->PriorSale->RecordingDate = date('Y/m/d', strtotime($data->Record->PriorSale->RecordingDate));
    $data->Record->PriorSale->SaleDate = date('Y/m/d', strtotime($data->Record->PriorSale->SaleDate));

    $data->Record->CurrentSale->SalePrice = str_replace('USD ', '$', money_format('%i', (double) $data->Record->CurrentSale->SalePrice));
    $data->Record->PriorSale->SalePrice = str_replace('USD ', '$', money_format('%i', (double) $data->Record->PriorSale->SalePrice));
    $data->Record->PriorSale->MortgageAmount = str_replace('USD ', '$', money_format('%i', (double) $data->Record->PriorSale->MortgageAmount));

    $suf = ' sq ft';
    $data->Record->Lot->SquareFootage = number_format((int) $data->Record->Lot->SquareFootage) . $suf;
    $data->Record->SquareFootage->UniversalBuilding = number_format((int) $data->Record->SquareFootage->UniversalBuilding) . $suf;
    $data->Record->SquareFootage->BuildingArea = number_format((int) $data->Record->SquareFootage->BuildingArea) . $suf;
    $data->Record->SquareFootage->LivingSpace = number_format((int) $data->Record->SquareFootage->LivingSpace) . $suf;

    $garageCodes = [
        '001' => 'Undefined Type',
        '002' => 'Undefined Type – 2 Car',
        '003' => 'Undefined Type – 3 Car',
        '004' => 'Undefined Type – 4 Car',
        '005' => 'Undefined Type – 5 Car',
        '006' => 'Undefined Type – 6 Car',
        '010' => 'Attached Garage/Carport',
        '020' => 'Attached Basement',
        '030' => 'Detached Basement',
        '040' => 'Detached Garage/Carport',
        '050' => 'Enclosed Brick Garage/Carport',
        '060' => 'Basement Finished',
        '061' => 'Finished Basement – 1 Car',
        '062' => 'Finished Basement – 2 Car',
        '063' => 'Finished Basement – 3 Car',
        '064' => 'Finished Basement – 4 Car',
        '070' => 'Finished Built In',
        '080' => 'Unfinished Basement',
        '081' => 'Unfinished Basement – 1 Car',
        '082' => 'Unfinished Basement – 2 Car',
        '083' => 'Unfinished Basement – 3 Car',
        '084' => 'Unfinished Basement – 4 Car',
        '090' => 'Unfinished Built In'
    ];
    if (array_key_exists((string) $data->Record->Building->GarageCode, $garageCodes)) {
        $data->Record->Building->GarageType = $garageCodes[$data->Record->Building->GarageCode];
    } else {
        $data->Record->Building->GarageType = 'Undefined Type';
    }

    $parkingCode = [
        '001' => 'Type Unknown',
        '002' => 'Undefined Type – 2 Car Garage',
        '003' => 'Undefined Type – 3 Car Garage',
        '004' => 'Undefined Type – 4 Car Garage',
        '005' => 'Undefined Type – 5 Car Garage',
        '006' => 'Undefined Type – 6 Car Garage',
        '010' => 'Attached Garage/Carport',
        '020' => 'Attached Basement Garage',
        '030' => 'Detached Basement Garage',
        '040' => 'Detached Garage/Carport',
        '050' => 'Enclosed Brick Garage/Carport',
        '060' => 'Basement Finished Garage',
        '061' => 'Finished Basement – 1 Car Garage',
        '062' => 'Finished Basement – 2 Car Garage',
        '063' => 'Finished Basement – 3 Car Garage',
        '064' => 'Finished Basement – 4 Car Garage',
        '070' => 'Finished Built In Garage',
        '080' => 'Unfinished Basement Garage',
        '081' => 'Unfinished Basement – 1 Car Garage',
        '082' => 'Unfinished Basement – 2 Car Garage',
        '083' => 'Unfinished Basement – 3 Car Garage',
        '084' => 'Unfinished Basement – 4 Car Garage',
        '090' => 'Unfinished Built In Garage',
        '100' => 'Prefab Garage',
        '110' => 'Basement Garage',
        '111' => 'Basement – 1 Car Garage',
        '112' => 'Basement – 2 Car Garage',
        '113' => 'Basement – 3 Car Garage',
        '114' => 'Basement – 4 Car Garage',
        '115' => 'Basement – 5 Car Garage',
        '116' => 'Basement – 6 Car Garage',
        '120' => 'Built-In Garage',
        '121' => 'Built-In – 1 Car Garage',
        '122' => 'Built-In – 2 Car Garage',
        '130' => 'Built Under Garage',
        '140' => 'Garage/Carport',
        '150' => 'Cinderblock Garage',
        '160' => 'Asbestos Garage',
        '200' => 'Finished Attached Masonry Garage',
        '210' => 'Unfinished Attached Masonry Garage'
    ];
    if (array_key_exists((string) $data->Record->Building->ParkingCode, $parkingCode)) {
        $data->Record->Building->ParkingType = $parkingCode[(string) $data->Record->Building->ParkingCode];
    } else {
        $data->Record->Building->ParkingType = 'Type Unknown';
    }

    return $data;
}

function mortgage(Application $app, Request $request)
{
    /** @var Form $form */
    $mortgageForm = $app['form.factory']
        ->createBuilder(new MortgageForm(null, range(0, 2)))
        ->getForm();

    $session = $app['session'];
    $property = $session->get('property');
    $zillow = new ZillowApi($app['zwsid']);


    $mortgageForm->bind($request);

    if ($mortgageForm->isValid()) {

        $data = $mortgageForm->getData();

        $mortgageXml = $zillow->GetMonthlyPayments([
            'zpid' => $property->zpid,
            'price' => $data['cost'],
            'down' => $data['percentDown'],
            'zip' => $property->address->zipcode
        ]);

        $response = new Response();

        if ($mortgageXml->xpath('response')) {

            $mortgage = json_decode(json_encode($mortgageXml->xpath('response')[0]));

            if ($mortgage) {

                $app['mortgage'] = $mortgage;
                $app['mortgage']->payment = $mortgage->payment[$data['loanType']];

                $response = new Response(json_encode($app['mortgage']));
            }
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    return invalidFormResponse($app, $mortgageForm);
}

function report_missing_info(Application $app, Request $request)
{
    /** @var Form $form */
    $reportMissingInfoForm = $app['form.factory']
        ->createBuilder(new ReportMissingInfoForm())
        ->getForm();

    $reportMissingInfoForm->submit($request);

    $result = [];
    if ($reportMissingInfoForm->isValid()) {

        try {
            $message = \Swift_Message::newInstance()
                ->setSubject('homereports.org report missing info.')
                ->setFrom(array($app['session']->get('customerData')['emailAddress']))
                ->setTo(array('contact@homereports.org'))
                ->setBody($reportMissingInfoForm->getData()['message']);

            $app['mailer']->send($message);
        } catch (Exception $e) {
            if (get_class($e) === 'Swift_RfcComplianceException') {
                $result[] = ['key' => 'Email', 'val' => 'Invalid email address.'];
            }
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
    } else {
        foreach ($reportMissingInfoForm->all() as $key => $child) {
            if ($child->getErrors()) {
                $result[] = ['key' => ucfirst($key), 'val' => $child->getErrors()[0]->getMessage()];
            }
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
    }

    return $response;
}

function invalidFormResponse(Application $app, FormInterface $form, $formTmpl = null)
{
    $result = ['errors' => []];

    foreach ($form->getErrors() as $error) {
        $result['errors']['form'][] = $error->getMessage();
    }

    $result['errors'] += getChildErrors($form);

    if ($extra = $form->getExtraData()) {
        $result['errors']['extraData'] = $extra;
    }

    if ($formTmpl) {
        $result['rendered'] = $app['twig']->render($formTmpl, ['form' => $form->createView()])->getContent();
    }

    $response = new Response(json_encode($result), 400);
    $response->headers->set('Content-Type', 'application/json');
    return $response;
}

function getChildErrors(FormInterface $form)
{

    $errors = [];

    foreach ($form->all() as $name => $child) {

        foreach ($child->getErrors() as $error) {
            $errors[$name][] = $error->getMessage();
        }

        if ($childErrors = getChildErrors($child)) {
            $errors[$name] = $childErrors;
        }
    }

    return $errors;
}
